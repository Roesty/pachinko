/*
 * MovingPlatform.cpp
 *
 *  Created on: Mar 14, 2022
 *      Author: mooseman
 */

#include "MovingPlatform.h"

MovingPlatform::MovingPlatform(b2World* world)
{
	m_width = 15.0f;
	m_height = 2.0f;

	m_mesh.reset(new Mesh);
	m_mesh->setToMovingPlatform(m_width, m_height, 480, 64);

	b2BodyDef bodyDef;
	bodyDef.type = b2_kinematicBody;

	m_physical.reset(new Physical(world, &bodyDef, m_mesh.get()));

    b2FixtureDef fixtureDef;
    fixtureDef.restitution = 0.3f;
    fixtureDef.friction = 0.1;
    fixtureDef.density = 3.0f;

    // Attach the polygons
    for (int polygon=0; polygon<m_physical->m_polygonShapes.size(); polygon++)
    {
    	fixtureDef.shape = m_physical->m_polygonShapes[polygon].get();
    	m_physical->m_body->CreateFixture(&fixtureDef);
    }

    // Attach the circles
    for (int circle=0; circle<m_physical->m_circleShapes.size(); circle++)
    {
    	fixtureDef.shape = m_physical->m_circleShapes[circle].get();
    	m_physical->m_body->CreateFixture(&fixtureDef);
    }

    // Attach the chains
    for (int chain=0; chain<m_physical->m_chainShapes.size(); chain++)
    {
    	fixtureDef.shape = m_physical->m_chainShapes[chain].get();
    	m_physical->m_body->CreateFixture(&fixtureDef);
    }

	m_startingPosition = glm::vec3(100.0f, -200.0f, 0.0f);
	m_startingPosition.x -= 10.0f+m_width;

	m_resetPosition = m_startingPosition;

	setMPosition(glm::vec4(m_startingPosition.x, m_startingPosition.y, m_startingPosition.z, 0.0f));

	velocity = b2Vec2(0.0f, 2500.0f);

	boundary = glm::vec2(0.0f, 550.0f);
	boundary = glm::vec2(0.0f, 590.0f);

	m_physical->m_body->SetLinearVelocity(velocity);
}

MovingPlatform::~MovingPlatform() {
	// TODO Auto-generated destructor stub
}

void MovingPlatform::act()
{
	float xTravelled = m_physical->m_body->GetTransform().p.x - m_startingPosition.x;
	float yTravelled = m_physical->m_body->GetTransform().p.y - m_startingPosition.y;

	b2Transform resetPosition;
	resetPosition.q = m_physical->m_body->GetTransform().q;
	resetPosition.p.x = m_resetPosition.x;
	resetPosition.p.y = m_resetPosition.y;

	if (xTravelled > (boundary.x+1.0f) || yTravelled > (boundary.y+1.0f))
	{
		m_physical->m_body->SetTransform(resetPosition.p, resetPosition.q.GetAngle());
	}
}































