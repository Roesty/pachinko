#ifndef CAMERA_CLASS_H
#define CAMERA_CLASS_H

#define GLM_ENABLE_EXPERIMENTAL

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/vector_angle.hpp>

#include <SDL2/SDL.h>

#include "Shader.h"

class Camera
{
public:
    // Stores the main vectors of the camera
    glm::vec3 m_position;
    glm::vec3 m_orientation;
    glm::vec3 m_up;

    // Stores the view and projection matrices
    glm::mat4 m_view = glm::mat4(1.0f);

    // For camera control
    GLfloat m_yaw, m_pitch;

    // Prevents the camera from jumping around when first clicking left click
    bool firstClick = true;

    // FOV constraints
	GLfloat m_minFOV;
	GLfloat m_maxFOV;
	GLfloat m_curFOV;

	//Distance contraints
	GLfloat m_near;
	GLfloat m_far;

	// Used for all movements to the camera
	GLfloat m_deltaTime;

    // Stores the width and height of the window
    int m_width;
    int m_height;
    void updateWidthAndHeight(int width, int height);

    // Adjust the speed of the camera and it's sensitivity when looking around
    float m_speed;
    float m_sensitivity;
    float m_zoomSensitivity;

    //When moving the camera fast, the movement speed is multiplied with this value
    float m_fastCameraMultiplier;

	// Camera constructor to set up initial values
    Camera(int width, int height, glm::vec3 position);
    Camera();
    void setup(int width, int height, glm::vec3 position);

    void calculateView();

	// Updates the camera matrix to the Vertex Shader
    void updateMatrix(float FOVdeg, float nearPlane, float farPlane);
    void Matrix(Shader& shader, const char* uniform);

    void exportToShader(Shader& shader);

    glm::mat4 getView();
    glm::mat4 getProjection();

    // Handles camera inputs
    void handleInputs(SDL_Event* event);

    //Handles camera rotation (mouse input)
    void handleMouseInput(SDL_Event* event);

    //Handles camera movement (keyboard input)
    void handleKeyboardInput();

	GLfloat getMCurFov() const {
		return m_curFOV;
	}

	void setMCurFov(GLfloat mCurFov = 90.0f) {
		m_curFOV = mCurFov;
	}

	GLfloat getMMaxFov() const {
		return m_maxFOV;
	}

	void setMMaxFov(GLfloat mMaxFov = 120.0f) {
		m_maxFOV = mMaxFov;
	}

	GLfloat getMMinFov() const {
		return m_minFOV;
	}

	void setMMinFov(GLfloat mMinFov = 10.0f) {
		m_minFOV = mMinFov;
	}

	const glm::vec3& getMOrientation() const {
		return m_orientation;
	}

	void setMOrientation(
			const glm::vec3 &mOrientation = glm::vec3(0.0f, 0.0f, -1.0f)) {
		m_orientation = mOrientation;
	}

	GLfloat getMPitch() const {
		return m_pitch;
	}

	void setMPitch(GLfloat mPitch) {
		m_pitch = mPitch;
	}

	const glm::vec3& getMPosition() const {
		return m_position;
	}

	void setMPosition(const glm::vec3 &mPosition) {
		m_position = mPosition;
	}

	float getMSensitivity() const {
		return m_sensitivity;
	}

	void setMSensitivity(float mSensitivity = 100.0f) {
		m_sensitivity = mSensitivity;
	}

	float getMSpeed() const {
		return m_speed;
	}

	void setMSpeed(float mSpeed = 0.1f) {
		m_speed = mSpeed;
	}

	const glm::vec3& getMUp() const {
		return m_up;
	}

	void setMUp(const glm::vec3 &mUp = glm::vec3(0.0f, 1.0f, 0.0f)) {
		m_up = mUp;
	}

	GLfloat getMYaw() const {
		return m_yaw;
	}

	void setMYaw(GLfloat mYaw) {
		m_yaw = mYaw;
	}

	float getMZoomSensitivity() const {
		return m_zoomSensitivity;
	}

	void setMZoomSensitivity(float mZoomSensitivity = 100.0f) {
		m_zoomSensitivity = mZoomSensitivity;
	}

	GLfloat getMDeltaTime() const {
		return m_deltaTime;
	}

	void setMDeltaTime(GLfloat mDeltaTime) {
		m_deltaTime = mDeltaTime;
	}
};
#endif
