#include "MeshFactory.h"

MeshFactory::MeshFactory()
{
	partResolution = 100;
}


Mesh MeshFactory::getGroundFloor(int length, int width, float size, glm::vec3 c1, glm::vec3 c2)
{
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;

	for (int x = 0; x < length; x++)
	{
		for (int y = 0; y < width; y++)
		{
			indices.push_back(vertices.size());
			indices.push_back(vertices.size() + 1);
			indices.push_back(vertices.size() + 2);
			indices.push_back(vertices.size());
			indices.push_back(vertices.size() + 2);
			indices.push_back(vertices.size() + 3);

			float xOffset = (length * size) / 2;
			float yOffset = (width * size) / 2;

			float x1 = x * size - xOffset;
			float x2 = x * size + size - yOffset;

			float z1 = y * size - xOffset;
			float z2 = y * size + size - yOffset;

			bool colorSelect = x % 2;

			if (y % 2)
			{
				colorSelect = !colorSelect;
			}

			glm::vec3 color;

			if (colorSelect)
			{
				color = c1;
			} else
			{
				color = c2;
			}

			glm::vec3 position(x1, 0, z1);
			glm::vec2 texPosition(0, 0);
			glm::vec3 normal(0, 1, 0);

			vertices.push_back(
			{position, normal, color, texPosition});

			position.x = x2;
			texPosition.x = 1.0f;
			vertices.push_back(
			{position, normal, color, texPosition});

			position.z = z2;
			texPosition.y = 1.0f;
			vertices.push_back(
			{position, normal, color, texPosition});

			position.x = x1;
			texPosition.x = 0.0f;
			vertices.push_back(
			{position, normal, color, texPosition});
		}
	}

	return Mesh(vertices, indices);
}

std::shared_ptr<Mesh> MeshFactory::getGroundMesh(int length, int width, float size, glm::vec3 c1, glm::vec3 c2)
{
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;

	for (int x = 0; x < length; x++)
	{
		for (int y = 0; y < width; y++)
		{
			indices.push_back(vertices.size());
			indices.push_back(vertices.size() + 1);
			indices.push_back(vertices.size() + 2);
			indices.push_back(vertices.size());
			indices.push_back(vertices.size() + 2);
			indices.push_back(vertices.size() + 3);

			float xOffset = (length * size) / 2;
			float yOffset = (width * size) / 2;

			float x1 = x * size - xOffset;
			float x2 = x * size + size - yOffset;

			float z1 = y * size - xOffset;
			float z2 = y * size + size - yOffset;

			bool colorSelect = x % 2;

			if (y % 2)
			{
				colorSelect = !colorSelect;
			}

			glm::vec3 color;

			if (colorSelect)
			{
				color = c1;
			} else
			{
				color = c2;
			}

			glm::vec3 position(x1, 0, z1);
			glm::vec2 texPosition(0, 0);
			glm::vec3 normal(0, 1, 0);

			vertices.push_back(
			{position, normal, color, texPosition});

			position.x = x2;
			texPosition.x = 1.0f;
			vertices.push_back(
			{position, normal, color, texPosition});

			position.z = z2;
			texPosition.y = 1.0f;
			vertices.push_back(
			{position, normal, color, texPosition});

			position.x = x1;
			texPosition.x = 0.0f;
			vertices.push_back(
			{position, normal, color, texPosition});
		}
	}

	return std::make_shared<Mesh>(vertices, indices);
}
