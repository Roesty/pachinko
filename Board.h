/*
 * Board.h
 *
 *  Created on: Mar 14, 2022
 *      Author: mooseman
 */

#ifndef BOARD_H_
#define BOARD_H_

#include "Entity.h"
#include "Mesh.h"

class Board: public Entity {
public:
	Board(b2World* world);
	virtual ~Board();

	std::vector<b2Vec2> getOuter();
	std::vector<b2Vec2> getInner();
};

#endif /* BOARD_H_ */
