/*
 * Physical.h
 *
 *  Created on: Mar 1, 2022
 *      Author: mooseman
 */

#ifndef PHYSICAL_H_
#define PHYSICAL_H_

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/common.hpp>

#include <Box2D/Box2D.h>

#include "Mesh.h"

class Physical {
public:

	//Pointer to the world it belongs
	b2World* m_world;

	b2Body* m_body;
//	b2Body* m_body = world.CreateBody(&m_bodyDef);

	//The shapes of this physical. These are all added to the fixture
	std::vector<std::shared_ptr<b2PolygonShape>> m_polygonShapes;
	std::vector<std::shared_ptr<b2CircleShape>> m_circleShapes;
	std::vector<std::shared_ptr<b2ChainShape>> m_chainShapes;

//	std::shared_ptr<b2ChainShape> m_shape;
	b2ChainShape m_shape;

//	std::vector<std::shared_ptr<b2Shape>> m_shapes;
	std::vector<b2PolygonShape> m_shapes;

//	m_body->CreateFixture(&m_shape, 0.0f);

	Physical();
	Physical(b2World* world, b2BodyDef* bodyDef, std::vector<b2Vec2>* vertices);
	Physical(b2World* world, b2BodyDef* bodyDef, std::vector<b2PolygonShape>* boxes);
	Physical(b2World* world, b2BodyDef* bodyDef, Mesh* mesh);
//	Physical(b2World* world, b2BodyDef* bodyDef, std::vector<std::shared_ptr<b2Shape>> boxes);
	virtual ~Physical();

	glm::vec4 getPosition();
	void setPosition(glm::vec4 position);
	void setPosition(float x, float y);
};

#endif /* PHYSICAL_H_ */
