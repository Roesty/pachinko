/*
 * Entity.h
 *
 *  Created on: Dec 22, 2021
 *      Author: mooseman
 */

#include <memory>
#include <glm/common.hpp>

#include "Shader.h"
#include "Camera.h"

#include "Mesh.h"
#include "Physical.h"

#ifndef ENTITY_H_
#define ENTITY_H_

class Entity
{
public:
	//The mesh for this entity
	std::shared_ptr<Mesh> m_mesh;

	//The physics of this entity
	std::shared_ptr<Physical> m_physical;

	//The position. This is only used if the entity is not represented by a physical
	glm::vec4 m_position;

	//True if this entity is supposed to be drawn
	bool m_draw;

	//debug
	float m_debugAngle;

	//Run this every frame
	void act();

//	glm::vec4 test(0.0f, 0.0f, 0.0f, 0.0f);

public:

	void updateBody(unsigned int VBO, b2PolygonShape body);

	//Draw this entity
	void draw(Shader shader);

	Entity();
	virtual ~Entity();
	bool isMDraw() const;
	void setMDraw(bool mDraw);

	const std::shared_ptr<Mesh>& getMMesh() const;
	void setMMesh(const std::shared_ptr<Mesh> mMesh);

	const glm::vec4& getMPosition() const;
	void setMPosition(const glm::vec4& mPosition);

};

#endif /* ENTITY_H_ */
