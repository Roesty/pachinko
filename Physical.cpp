/*
 * Physical.cpp
 *
 *  Created on: Mar 1, 2022
 *      Author: mooseman
 */

#include "Physical.h"

Physical::Physical(b2World* world, b2BodyDef* bodyDef, std::vector<b2Vec2>* vertices)
{
	m_world = world;

	m_body = world->CreateBody(bodyDef);

//	m_shape.reset(new b2ChainShape);

//	std::cout<<"vertices in loopcreation: "<<vertices->size()<<std::endl;
//	std::cout<<"Vertex count: "<<m_shape.m_count<<std::endl;
	m_shape.CreateLoop(vertices->data(), vertices->size());
//	std::cout<<"Vertex count: "<<m_shape.m_count<<std::endl;
//	m_shape->CreateLoop(mesh->m_physicalVertices.data(), mesh->m_physicalVertices.size());
}

Physical::Physical(b2World *world, b2BodyDef *bodyDef, std::vector<b2PolygonShape>* boxes)
//Physical::Physical(b2World *world, b2BodyDef *bodyDef, std::vector<std::shared_ptr<b2Shape>> boxes)
{
	m_world = world;

	m_body = world->CreateBody(bodyDef);

//	std::cout<<"vertices in loopcreation: "<<vertices->size()<<std::endl;
//	std::cout<<"Vertex count: "<<m_shape.m_count<<std::endl;

//	m_shape.CreateLoop(vertices->data(), vertices->size());

//	m_shapes = boxes;

	std::cout<<"Shapes size: "<<m_shapes.size()<<std::endl;
	for (int box=0; box<boxes->size(); box++)
	{
		m_shapes.push_back(boxes->at(box));
	}
	std::cout<<"Shapes size: "<<m_shapes.size()<<std::endl;

	//	std::cout<<"Vertex count: "<<m_shape.m_count<<std::endl;
//	m_shape->CreateLoop(mesh->m_physicalVertices.data(), mesh->m_physicalVertices.size());
}

Physical::Physical(b2World *world, b2BodyDef *bodyDef, Mesh* mesh)
{
	m_world = world;

	m_body = world->CreateBody(bodyDef);

    // Attach the polygons
    for (int polygon=0; polygon<mesh->m_polygonShapes.size(); polygon++)
    {
    	m_polygonShapes.push_back(mesh->m_polygonShapes[polygon]);
    }

    // Attach the circles
    for (int circle=0; circle<mesh->m_circleShapes.size(); circle++)
    {
    	m_circleShapes.push_back(mesh->m_circleShapes[circle]);
//    	std::cout<<"circle!"<<std::endl;
    }

    // Attach the chains
    for (int chain=0; chain<mesh->m_chainShapes.size(); chain++)
    {
    	m_chainShapes.push_back(mesh->m_chainShapes[chain]);
    }
}

Physical::Physical()
{

}

Physical::~Physical()
{
	// TODO Auto-generated destructor stub
}

glm::vec4 Physical::getPosition()
{
	std::cout<<"bruh4"<<std::endl;
	float x = m_body->GetTransform().p.x;
	float x2 = x;
	std::cout<<"bruh5 "<<x<<std::endl;
	float y = m_body->GetPosition().y;
	std::cout<<"bruh6 "<<y<<std::endl;

	return glm::vec4(x, y, 0.0f, 1.0f);
}

void Physical::setPosition(glm::vec4 position)
{
	float x = position.x;
	float y = position.y;

	float angle = m_body->GetAngle();
	m_body->SetTransform(b2Vec2(x, y), angle);
}

void Physical::setPosition(float x, float y)
{
	setPosition(glm::vec4(x, y, 0, 0));
}
