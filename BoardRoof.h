/*
 * BoardRoof.h
 *
 *  Created on: Mar 15, 2022
 *      Author: mooseman
 */

#ifndef BOARDROOF_H_
#define BOARDROOF_H_

#include "Entity.h"

class BoardRoof: public Entity {
public:
	BoardRoof(b2World* world);

	virtual ~BoardRoof();
};

#endif /* BOARDROOF_H_ */
