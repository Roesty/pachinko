/*
 * Peg.cpp
 *
 *  Created on: Mar 2, 2022
 *      Author: mooseman
 */

#include "Peg.h"

#include <iostream>

Peg::Peg(b2World* world)
{
	m_mesh.reset(new Mesh);
	m_mesh->setToPeg(0.6f, 480, 64);

	b2BodyDef bodyDef;
	bodyDef.type = b2_staticBody;
//	bodyDef.type = b2_dynamicBody;

//	m_physical.reset(new Physical(world, &bodyDef, &m_mesh->m_physicalVertices));
	m_physical.reset(new Physical(world, &bodyDef, m_mesh.get()));

    b2FixtureDef fixtureDef;
    fixtureDef.shape = &m_physical->m_shape;
    fixtureDef.restitution = 0.2f;
    fixtureDef.friction = 0.1f;
    fixtureDef.density = 10.0f;

    for (int box=0; box<m_physical->m_shapes.size(); box++)
    {
    	fixtureDef.shape = &m_physical->m_shapes.at(box);
    	m_physical->m_body->CreateFixture(&fixtureDef);
    }

    // Attach the polygons
    for (int polygon=0; polygon<m_physical->m_polygonShapes.size(); polygon++)
    {
    	fixtureDef.shape = m_physical->m_polygonShapes[polygon].get();
    	m_physical->m_body->CreateFixture(&fixtureDef);
    }

    // Attach the circles
    for (int circle=0; circle<m_physical->m_circleShapes.size(); circle++)
    {
    	fixtureDef.shape = m_physical->m_circleShapes[circle].get();
    	m_physical->m_body->CreateFixture(&fixtureDef);
    }

    // Attach the chains
    for (int chain=0; chain<m_physical->m_chainShapes.size(); chain++)
    {
    	fixtureDef.shape = m_physical->m_chainShapes[chain].get();
    	m_physical->m_body->CreateFixture(&fixtureDef);
    }
}

Peg::~Peg() {
	// TODO Auto-generated destructor stub
}

void Peg::rotate(float angle)
{
	m_debugAngle += angle;
}





























