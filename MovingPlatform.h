/*
 * MovingPlatform.h
 *
 *  Created on: Mar 14, 2022
 *      Author: mooseman
 */

#ifndef MOVINGPLATFORM_H_
#define MOVINGPLATFORM_H_

#include "Entity.h"

class MovingPlatform: public Entity {
public:

	float m_width, m_height;

	//Where the platforms starts
	glm::vec3 m_startingPosition;

	//Where the platform will reset to
	glm::vec3 m_resetPosition;

	//How this platform moves every second
	b2Vec2 velocity;

	//How much the platform can move in each direction before being reset
	glm::vec2 boundary;

	void act();

	MovingPlatform(b2World* world);
	virtual ~MovingPlatform();
};

#endif /* MOVINGPLATFORM_H_ */
