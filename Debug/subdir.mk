################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Ball.cpp \
../Board.cpp \
../BoardRoof.cpp \
../Camera.cpp \
../Entity.cpp \
../Mesh.cpp \
../MeshFactory.cpp \
../MovingPlatform.cpp \
../Peg.cpp \
../Physical.cpp \
../Shader.cpp \
../Triangulate.cpp \
../VAO.cpp \
../VBO.cpp \
../main.cpp 

CPP_DEPS += \
./Ball.d \
./Board.d \
./BoardRoof.d \
./Camera.d \
./Entity.d \
./Mesh.d \
./MeshFactory.d \
./MovingPlatform.d \
./Peg.d \
./Physical.d \
./Shader.d \
./Triangulate.d \
./VAO.d \
./VBO.d \
./main.d 

OBJS += \
./Ball.o \
./Board.o \
./BoardRoof.o \
./Camera.o \
./Entity.o \
./Mesh.o \
./MeshFactory.o \
./MovingPlatform.o \
./Peg.o \
./Physical.o \
./Shader.o \
./Triangulate.o \
./VAO.o \
./VBO.o \
./main.o 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -IOpenGL -Iimgui -IGLM -IGLEW -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean--2e-

clean--2e-:
	-$(RM) ./Ball.d ./Ball.o ./Board.d ./Board.o ./BoardRoof.d ./BoardRoof.o ./Camera.d ./Camera.o ./Entity.d ./Entity.o ./Mesh.d ./Mesh.o ./MeshFactory.d ./MeshFactory.o ./MovingPlatform.d ./MovingPlatform.o ./Peg.d ./Peg.o ./Physical.d ./Physical.o ./Shader.d ./Shader.o ./Triangulate.d ./Triangulate.o ./VAO.d ./VAO.o ./VBO.d ./VBO.o ./main.d ./main.o

.PHONY: clean--2e-

