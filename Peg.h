/*
 * Peg.h
 *
 *  Created on: Mar 2, 2022
 *      Author: mooseman
 */

#ifndef PEG_H_
#define PEG_H_

#include "Entity.h"
#include "Mesh.h"

class Peg: public Entity {
public:
	Peg(b2World* world);
	virtual ~Peg();

	void rotate(float angle);
};

#endif /* PEG_H_ */
