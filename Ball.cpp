/*
 * Ball.cpp
 *
 *  Created on: Mar 5, 2022
 *      Author: mooseman
 */

#include "Ball.h"

#include <iostream>

Ball::Ball(b2World* world)
{
	m_mesh.reset(new Mesh);
	m_mesh->setToBall3(1.0f, 480, 64);

	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;

//	m_physical.reset(new Physical(world, &bodyDef, &m_mesh->m_physicalVertices));
//	m_physical.reset(new Physical(world, &bodyDef, m_mesh->m_boxes));
//	m_physical.reset(new Physical(world, &bodyDef, &m_mesh->m_boxes));
	m_physical.reset(new Physical(world, &bodyDef, m_mesh.get()));

//    b2FixtureDef fixtureDef;
//    fixtureDef.shape = &m_physical->m_shape;
//    fixtureDef.restitution = 0.5f;
//    fixtureDef.friction = 0.5;
//    fixtureDef.density = 1.0f;
//
//    std::cout<<"Ball vertices: "<<m_physical->m_shape.m_count<<std::endl;
//
//    m_physical->m_body->CreateFixture(&fixtureDef);

	//https://stackoverflow.com/questions/4706484/box2d-multiple-fixtures-and-positioning
	//https://www.iforce2d.net/b2dtut/fixtures

    b2FixtureDef fixtureDef;
    fixtureDef.restitution = 0.3f;
    fixtureDef.friction = 0.2;
    fixtureDef.density = 10.0f;

    for (int box=0; box<m_physical->m_shapes.size(); box++)
    {
//    	fixtureDef.shape = m_physical->m_shapes[box].get();
    	fixtureDef.shape = &m_physical->m_shapes.at(box);

    	m_physical->m_body->CreateFixture(&fixtureDef);
    }
//    std::cout<<"Ball vertices: "<<m_physical->m_shape.m_count<<std::endl;

    // Attach the polygons
    for (int polygon=0; polygon<m_physical->m_polygonShapes.size(); polygon++)
    {
    	fixtureDef.shape = m_physical->m_polygonShapes[polygon].get();
    	m_physical->m_body->CreateFixture(&fixtureDef);
    }

    // Attach the circles
    for (int circle=0; circle<m_physical->m_circleShapes.size(); circle++)
    {
    	fixtureDef.shape = m_physical->m_circleShapes[circle].get();
    	m_physical->m_body->CreateFixture(&fixtureDef);
    }

    // Attach the chains
    for (int chain=0; chain<m_physical->m_chainShapes.size(); chain++)
    {
    	fixtureDef.shape = m_physical->m_chainShapes[chain].get();
    	m_physical->m_body->CreateFixture(&fixtureDef);
    }

//    b2FixtureDef fixtureDef;
//    fixtureDef.shape = m_physical->m_shape.get();
//    fixtureDef.restitution = 0.5f;
//    std::cout<<"Ball vertices: "<<m_physical->m_shape->m_count<<std::endl;
//
//    m_physical->m_body->CreateFixture(m_physical->m_shape);

}

Ball::~Ball() {
	// TODO Auto-generated destructor stub
}
