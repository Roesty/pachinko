/*
 * Board.cpp
 *
 *  Created on: Mar 14, 2022
 *      Author: mooseman
 */

#include "Board.h"

#include <iostream>

Board::Board(b2World* world)
{
	m_mesh.reset(new Mesh);
	m_mesh->setToBoard(1.0f, 480, 64);

	b2BodyDef bodyDef;
	bodyDef.type = b2_staticBody;

//	m_physical.reset(new Physical(world, &bodyDef, &m_mesh->m_physicalVertices));
	m_physical.reset(new Physical(world, &bodyDef, m_mesh.get()));

    b2FixtureDef fixtureDef;
    fixtureDef.shape = &m_physical->m_shape;
    fixtureDef.restitution = 0.3f;
    fixtureDef.friction = 0.05f;
    fixtureDef.density = 10.0f;

    for (int box=0; box<m_physical->m_shapes.size(); box++)
    {
    	fixtureDef.shape = &m_physical->m_shapes.at(box);
    	m_physical->m_body->CreateFixture(&fixtureDef);
    }

    // Attach the polygons
    for (int polygon=0; polygon<m_physical->m_polygonShapes.size(); polygon++)
    {
    	fixtureDef.shape = m_physical->m_polygonShapes[polygon].get();
    	m_physical->m_body->CreateFixture(&fixtureDef);
    }

    // Attach the circles
    for (int circle=0; circle<m_physical->m_circleShapes.size(); circle++)
    {
    	fixtureDef.shape = m_physical->m_circleShapes[circle].get();
    	m_physical->m_body->CreateFixture(&fixtureDef);
    }

    // Attach the chains
    for (int chain=0; chain<m_physical->m_chainShapes.size(); chain++)
    {
    	fixtureDef.shape = m_physical->m_chainShapes[chain].get();
    	m_physical->m_body->CreateFixture(&fixtureDef);
    }

}

Board::~Board() {
	// TODO Auto-generated destructor stub
}

std::vector<b2Vec2> Board::getOuter() {
}

std::vector<b2Vec2> Board::getInner() {
}
