
// To compile with gcc:  (tested on Ubuntu 14.04 64bit):
//	 g++ sdl2_opengl.cpp -lSDL2 -lGL
// To compile with msvc: (tested on Windows 7 64bit)
//   cl sdl2_opengl.cpp /I C:\sdl2path\include /link C:\path\SDL2.lib C:\path\SDL2main.lib /SUBSYSTEM:CONSOLE /NODEFAULTLIB:libcmtd.lib opengl32.lib

#include <GL/glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/common.hpp>

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#include <Box2D/Box2D.h>

//#include <imgui/backends/imgui_impl_sdl.h>

#include <vector>
#include <fstream>
#include <string>
#include <ctime>

#include "Shader.h"
#include <stb/stb_image.h>

#include "VAO.h"
#include "Camera.h"
#include "Mesh.h"
#include "MeshFactory.h"

#include "Entity.h"
#include "Peg.h"
#include "Ball.h"
#include "Board.h"
#include "BoardRoof.h"
#include "MovingPlatform.h"

enum eBackgroundtiles
{
	TILE_BLUE=0,
	TILE_ORANGE,
	TILE_GRAY,
	TILE_GREEN,
	TILE_PINK,
	TILE_PURPLE,
	TILE_YELLOW,

	MAX_BACKGROUND_TILE
};

int winWidth = 1600;
int winHeight = 1200;

void assignVAOstructure()
{
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(0 * sizeof(float)));
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(6 * sizeof(float)));
	glEnableVertexAttribArray(2);

	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(9 * sizeof(float)));
	glEnableVertexAttribArray(3);
}

std::vector<Vertex> getBackground(int width, int height, int spritesWidth, int spritesHeight, eBackgroundtiles backgroundTile)
{
	int area = width*height;
	std::vector<glm::vec3> positions;

	for (int row=0; row<width; row++)
	{
		for (int col=0; col<height; col++)
		{
			positions.push_back(glm::vec3(row, col, 0.0f));
			positions.push_back(glm::vec3(row+1, col, 0.0f));
			positions.push_back(glm::vec3(row, col+1, 0.0f));
			positions.push_back(glm::vec3(row+1, col+1, 0.0f));
		}
	}

	//	positions.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
	//	positions.push_back(glm::vec3(width, 0.0f, 0.0f));
	//	positions.push_back(glm::vec3(0.0f, height, 0.0f));
	//	positions.push_back(glm::vec3(width, height, 0.0f));

	std::vector<glm::vec3> colors;
	colors.push_back(glm::vec3(1.0f, 1.0f, 1.0f));

	colors[0] *= 0.6f;

	std::vector<glm::vec3> normals;
	normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));

	std::vector<glm::vec2> textureCoordinates;

	float left, right;
	left = backgroundTile*64;
	right = left+64;

	left /= spritesWidth;
	right /= spritesWidth;

	textureCoordinates.push_back(glm::vec2(left, 0.0f));
	textureCoordinates.push_back(glm::vec2(right, 0.0f));
	textureCoordinates.push_back(glm::vec2(left, 1.0f));
	textureCoordinates.push_back(glm::vec2(right, 1.0f));

	std::vector<Vertex> vertices;
	for (int tile = 0; tile<area; tile++)
	{
		for (int corner=0; corner<2; corner++)
		{
			vertices.push_back({positions[tile*4+corner+0], colors[0], normals[0], textureCoordinates[corner+0]});
			vertices.push_back({positions[tile*4+corner+1], colors[0], normals[0], textureCoordinates[corner+1]});
			vertices.push_back({positions[tile*4+corner+2], colors[0], normals[0], textureCoordinates[corner+2]});
		}
	}

	return vertices;
}

std::vector<Vertex> getBox(float width, float height, int spritesWidth, int spritesHeight, eBackgroundtiles backgroundTile)
{
	std::vector<glm::vec3> positions;

	positions.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
	positions.push_back(glm::vec3(width, 0.0f, 0.0f));
	positions.push_back(glm::vec3(0.0f, height, 0.0f));
	positions.push_back(glm::vec3(width, height, 0.0f));

	std::vector<glm::vec3> colors;
	colors.push_back(glm::vec3(1.0f, 1.0f, 1.0f));

	colors[0] *= 1.0f;

	std::vector<glm::vec3> normals;
	normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));

	std::vector<glm::vec2> textureCoordinates;

	float left, right;
	left = backgroundTile*64;
	right = left+64;

	left /= spritesWidth;
	right /= spritesWidth;

	textureCoordinates.push_back(glm::vec2(left, 0.0f));
	textureCoordinates.push_back(glm::vec2(right, 0.0f));
	textureCoordinates.push_back(glm::vec2(left, 1.0f));
	textureCoordinates.push_back(glm::vec2(right, 1.0f));

	std::vector<Vertex> vertices;
	for (int corner=0; corner<2; corner++)
	{
		vertices.push_back({positions[corner+0], colors[0], normals[0], textureCoordinates[corner+0]});
		vertices.push_back({positions[corner+1], colors[0], normals[0], textureCoordinates[corner+1]});
		vertices.push_back({positions[corner+2], colors[0], normals[0], textureCoordinates[corner+2]});
	}

	return vertices;
}

std::vector<Vertex> getGround(float width, float height, int spritesWidth, int spritesHeight)
{
	eBackgroundtiles backgroundTile = TILE_GREEN;

	std::vector<glm::vec3> positions;

	positions.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
	positions.push_back(glm::vec3(width, 0.0f, 0.0f));
	positions.push_back(glm::vec3(0.0f, height, 0.0f));
	positions.push_back(glm::vec3(width, height, 0.0f));

	std::vector<glm::vec3> colors;
	colors.push_back(glm::vec3(1.0f, 1.0f, 1.0f));

	colors[0] *= 1.0f;

	std::vector<glm::vec3> normals;
	normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));

	std::vector<glm::vec2> textureCoordinates;

	float left, right;
	left = backgroundTile*64;
	right = left+64;

	left /= spritesWidth;
	right /= spritesWidth;

	textureCoordinates.push_back(glm::vec2(left, 0.0f));
	textureCoordinates.push_back(glm::vec2(right, 0.0f));
	textureCoordinates.push_back(glm::vec2(left, 1.0f));
	textureCoordinates.push_back(glm::vec2(right, 1.0f));

	std::vector<Vertex> vertices;
	for (int corner=0; corner<2; corner++)
	{
		vertices.push_back({positions[corner+0], colors[0], normals[0], textureCoordinates[corner+0]});
		vertices.push_back({positions[corner+1], colors[0], normals[0], textureCoordinates[corner+1]});
		vertices.push_back({positions[corner+2], colors[0], normals[0], textureCoordinates[corner+2]});
	}

	return vertices;
}

int main (int ArgCount, char **Args)
{
	/////////////////////////////
	// Clock setup
	/////////////////////////////

	srand((unsigned int)time(NULL));

	/////////////////////////////
	// SDL and OpenGL Setup
	/////////////////////////////

	unsigned int windowFlags = SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE;
	std::string windowTitle = "Pachinko";
	SDL_Window *window = SDL_CreateWindow(windowTitle.c_str(), 1920+100+500, 100, winWidth, winHeight, windowFlags);
	SDL_GLContext context = SDL_GL_CreateContext(window);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);

	glEnable(GL_DEPTH_TEST);

	glewInit();

	int nrAttributes;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
	std::cout << "Maximum nr of vertex attributes supported: " << nrAttributes << std::endl;

	float backgroundDepth = -10.0f;
	float objectsDepth = -8.0f;
	objectsDepth = 0.0f;

	/////////////////////////////
	// STBI setup
	/////////////////////////////

	stbi_set_flip_vertically_on_load(true);

	/////////////////////////////
	// Box2D setup
	////////////////////////////

	float gravityY = -30.0;
	b2Vec2 gravity(0.0f, gravityY);
	b2World world(gravity);

	b2BodyDef staticBodyDef;
	staticBodyDef.position.Set(0.0f,  0.0f);

	//Staticbody is used to create all static bodies
	b2Body* staticBody = world.CreateBody(&staticBodyDef);

	int32 velocityIterations = 6;
	int32 positionIterations = 2;

	/////////////////////////////
	// Shader setup
	/////////////////////////////

	Shader defaultShader("default.vert", "default.frag");

	/////////////////////////////
	// Texture setup
	/////////////////////////////

	/////////////////////////////
	// Spritesheet

	unsigned int spriteSheetTexture;

	glGenTextures(1, &spriteSheetTexture);
	glBindTexture(GL_TEXTURE_2D, spriteSheetTexture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	// load image, create texture and generate mipmaps
	int spritesheetWidth, spritesheetHeight, nrChannels;

	unsigned char *data = stbi_load("resources/textures/spritesheet.png", &spritesheetWidth, &spritesheetHeight, &nrChannels, 0);
	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, spritesheetWidth, spritesheetHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cout << "Failed to load texture" << std::endl;
	}
	stbi_image_free(data);

	/////////////////////////////
	// Object setup
	/////////////////////////////

	/////////////////////////////
	// Background creation

//	int backgroundWidth = 160;
//	int backgroundHeight = 200;
//	std::vector<Vertex> background = getBackground(backgroundWidth, backgroundHeight, spritesheetWidth, spritesheetHeight, TILE_PURPLE);
//
//	glm::vec3 backgroundPosition(-backgroundWidth/2, -backgroundHeight/2, backgroundDepth);
//
//	unsigned int backgroundVAO, backgroundVBO, backgroundEBO;
//	glGenVertexArrays(1, &backgroundVAO);
//	glGenBuffers(1, &backgroundVBO);
//	glBindVertexArray(backgroundVAO);
//	glGenBuffers(1, &backgroundEBO);
//
//	glBindBuffer(GL_ARRAY_BUFFER, backgroundVBO);
//	glBufferData(GL_ARRAY_BUFFER, background.size()*sizeof(Vertex), background.data(), GL_STATIC_DRAW);
//
//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, backgroundEBO);
////	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(backgroundIndices), backgroundIndices, GL_STATIC_DRAW);
//	assignVAOstructure();

	/////////////////////////////
	// Peg creation

	std::vector<std::shared_ptr<Peg>> pegField;

	float startX = -70.0f;
	float startY = 200.0f;

	float endY = -30.0f;

	int columns = 15;
	int rows = 25;

	columns = 15;
	rows = 15;

	float pegFieldWidth = startX * 2;
	float pegFieldHeight = startY - endY;

	float pegFieldxOffset = -20.0f;
	float pegFieldyOffset = 0.0f;

	if (pegFieldWidth<0.0f)
	{
		pegFieldWidth *= -1.0f;
	}

	float spacingXCalculator = columns-1;
	float spacingYCalculator = rows-1;

	float spacingX = pegFieldWidth/spacingXCalculator;
	float spacingY = pegFieldHeight/spacingYCalculator;

	float halfSpacingX = spacingX/2.0f;

	for (int row=0; row<rows; row++)
	{
		for (int col=0; col<columns; col++)
		{
			//If the row is an odd number row, dont add the last peg
			if ((row % 2) && (col == columns-1))
			{
				break;
			}

			float x = startX;
			float y = startY;

			x += col*spacingX;
			y -= row*spacingY;

			x += pegFieldxOffset;
			y += pegFieldyOffset;

			if (row % 2)
			{
				x += halfSpacingX;
			}

			pegField.push_back(std::shared_ptr<Peg>(new Peg(&world)));
			pegField.back()->setMPosition(glm::vec4(x, y, 0, 0));
		}
	}

	std::cout<<"Expected pegs: "<<rows*columns<<std::endl;
	std::cout<<"World objects: "<<world.GetBodyCount()<<std::endl;

	/////////////////////////////
	// Box creation

	std::vector<std::shared_ptr<Ball>> ballField;

	float ballStartX = -65.0f;
	float ballStartY = 195.0f;

	float ballEndY = -25.0f;

	int ballColumns = 40;
	int ballRows = 40;

	float ballFieldWidth = ballStartX * 2;
	float ballFieldHeight = ballStartY - ballEndY;

	float ballFieldxOffset = -20.0f;
	float ballFieldyOffset = 0.0f;

	if (ballFieldWidth<0.0f)
	{
		ballFieldWidth *= -1.0f;
	}

	float ballSpacingXCalculator = ballColumns-1;
	float ballSpacingYCalculator = ballRows-1;

	float ballSpacingX = ballFieldWidth/ballSpacingXCalculator;
	float ballSpacingY = ballFieldHeight/ballSpacingYCalculator;

	for (int row=0; row<ballRows; row++)
	{
		for (int col=0; col<ballColumns; col++)
		{
			float x = ballStartX;
			float y = ballStartY;

			x += col*ballSpacingX;
			y -= row*ballSpacingY;

			x += ballFieldxOffset;
			y += ballFieldyOffset;

			ballField.push_back(std::shared_ptr<Ball>(new Ball(&world)));
			ballField.back()->setMPosition(glm::vec4(x, y, 0, 0));
		}
	}

//	b2BodyDef dynamixBoxBodyDef;
//	dynamixBoxBodyDef.type = b2_dynamicBody;
//	dynamixBoxBodyDef.position.Set(8.0f, 40.0f);
//
//	b2Body* dynamicBoxBody = world.CreateBody(&dynamixBoxBodyDef);
//
//	b2PolygonShape dynamicBoxShape;
//	dynamicBoxShape.SetAsBox(10.0f, 10.0f);
//
//	b2FixtureDef dynamicBoxFixtureDef;
//	dynamicBoxFixtureDef.shape = &dynamicBoxShape;
//	dynamicBoxFixtureDef.density = 2.0f;
//	dynamicBoxFixtureDef.friction = 0.3f;
//	dynamicBoxFixtureDef.restitution = 0.4f;
//
//	dynamicBoxBody->CreateFixture(&dynamicBoxFixtureDef);
//
//	std::vector<Vertex> box = getBox(20.0f, 20.0f, spritesheetWidth, spritesheetHeight, TILE_GRAY);
//
//	glm::vec3 boxPosition(0.0f, 0.0f, objectsDepth);
//
//	unsigned int boxVAO, boxVBO, boxEBO;
//	glGenVertexArrays(1, &boxVAO);
//	glGenBuffers(1, &boxVBO);
//	glBindVertexArray(boxVAO);
//	glGenBuffers(1, &boxEBO);
//
//	glBindBuffer(GL_ARRAY_BUFFER, boxVBO);
//	glBufferData(GL_ARRAY_BUFFER, box.size()*sizeof(Vertex), box.data(), GL_STATIC_DRAW);
//
//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, boxEBO);
//	assignVAOstructure();

	/////////////////////////////
	// Balls creation

//	int ballsRows = 10;
//	int ballsColumns = 50;
//
//	ballsRows = 20;
//	ballsColumns = 60;
//
//	int xOffset = -50;
//	int yOffset = 100;
//
//	float ballSpacingX = 1.0f;
//	float ballSpacingY = 1.0f;
//
//	std::vector<std::shared_ptr<Ball>> balls2;
//
//	for (int col=0; col<ballsColumns; col++)
//	{
////		break;
//		for (int row=0; row<ballsRows; row++)
//		{
//			int x = col + xOffset;
//			int y = row + yOffset;
//			int z = 0.0f;
//
//			x *= ballSpacingX;
//			y *= ballSpacingY;
//
//			balls2.push_back(std::shared_ptr<Ball>(new Ball(&world)));
//			balls2.back()->setMPosition(glm::vec4(x, y, z, 0.0f));
//		}
//	}

	/////////////////////////////
	// Moving platform creation

	int platformAmount = 10;
	std::vector<std::shared_ptr<MovingPlatform>> platforms;

	for (int iii=0; iii<platformAmount; iii++)
	{
		platforms.push_back(std::shared_ptr<MovingPlatform>(new MovingPlatform(&world)));

//		platforms.back()->resetPosition = glm::vec3(0.0f, 0.0f, 0.0f);
//		platforms.back()->boundary = glm::vec2(0.0f, 500.0f);

//		platforms.back()->startingPosition.x = platforms.back()->boundary.x*iii;
//		platforms.back()->startingPosition.y = platforms.back()->boundary.y*iii;

//		platforms.back()->setMPosition(glm::vec4(platforms.back()->startingPosition.x, platforms.back()->startingPosition.y, platforms.back()->startingPosition.z, 0.0f));

		glm::vec4 startingPosition;
		startingPosition.x = platforms.back()->m_startingPosition.x;
		startingPosition.y = platforms.back()->m_startingPosition.y;
		startingPosition.z = platforms.back()->m_startingPosition.z;
		startingPosition.w = 0.0f;

		float position = platformAmount;

		startingPosition.y += platforms.back()->boundary.y * ((float)iii/position);

		platforms.back()->setMPosition(startingPosition);
	}

//	MovingPlatform elevatorFloor(&world);

	/////////////////////////////
	// Ball2 creation

//	Ball ball2(&world);
//	ball2.setMPosition(glm::vec4(8.0f, 161.0f, 0, 0));

//	std::cout<<"######################"<<std::endl;
//	for (int coordinate=0; coordinate<ball2.m_physical->m_shape.m_count; coordinate++)
//	{
//		std::cout<<"Coordinate #"<<coordinate<<": ";
//		std::cout<<"x: "<<ball2.m_physical->m_shape.m_vertices[coordinate].x;
//		std::cout<<", y: "<<ball2.m_physical->m_shape.m_vertices[coordinate].y;
//		std::cout<<std::endl;
//	}

//	Ball ball3(&world);
//	ball3.setMPosition(glm::vec4(8.0f, 164.0f, 0, 0));

	/////////////////////////////
	// Board creation

	Board board(&world);
	board.setMPosition(glm::vec4(0.0f, 0.0f, 1.0f, 0.0f));

	/////////////////////////////
	// Boardroof creation

	BoardRoof boardRoof(&world);
	boardRoof.setMPosition(glm::vec4(0.0f, 300.0f, 1.0f, 0.0f));

	/////////////////////////////
	// Chainshape test creation

//	b2BodyDef chainShapeTestBodyDef;
//	chainShapeTestBodyDef.type = b2_staticBody;
//	chainShapeTestBodyDef.position.Set(0.0f, 25.0f);
//
//	b2Body* chainShapeTestBody = world.CreateBody(&chainShapeTestBodyDef);
//
//	b2ChainShape chainShapeTestShape;
//
//	std::vector<b2Vec2> points;
//	points.push_back({-1.0f, -1.0f});
//	points.push_back({1.0f, -1.0f});
//	points.push_back({1.0f, 1.0f});
//	points.push_back({-1.0f, 1.0f});
//	std::reverse(points.begin(), points.end());
//
//	chainShapeTestShape.CreateLoop(points.data(), points.size());
//
//    b2FixtureDef chainShapeTestFixtureDef;
//    chainShapeTestFixtureDef.shape = &chainShapeTestShape;
//    chainShapeTestFixtureDef.restitution = 0.5f;
//    chainShapeTestFixtureDef.friction = 0.5;
//    chainShapeTestFixtureDef.density = 1.0f;
//
//    chainShapeTestBody->CreateFixture(&chainShapeTestFixtureDef);

//	ballShape.m_p.Set(0, 0);
//	ballShape.m_radius = 2.0f;
//	int vertices = ballShape.GetVertexCount();

	/////////////////////////////
	// Ball creation

//	b2BodyDef ballBodyDef;
//	ballBodyDef.type = b2_dynamicBody;
//	ballBodyDef.position.Set(8.0f, 50.0f);
//	ballBodyDef.position.Set(8.0f, 150.0f);
//
//	b2Body* ballBody = world.CreateBody(&ballBodyDef);
//
//	b2CircleShape ballShape;
//	ballShape.m_p.Set(0, 0);
//	ballShape.m_radius = 4.0f;
//	int vertices = ballShape.GetVertexCount();
//
////	std::cout<<"ball vertices: "<<vertices<<std::endl;
//
//	b2FixtureDef ballFixtureDef;
//	ballFixtureDef.shape = &ballShape;
//	ballFixtureDef.density = 2.0f;
//	ballFixtureDef.friction = 0.3f;
//	ballFixtureDef.restitution = 0.6f;
//
//	ballBody->CreateFixture(&ballFixtureDef);
//
//	std::vector<Vertex> ball = getPeg(ballShape.m_radius, spritesheetWidth, spritesheetHeight);
//
//	glm::vec3 ballPosition(ballBodyDef.position.x, ballBodyDef.position.y, objectsDepth);
//
//	unsigned int ballVAO, ballVBO, ballEBO;
//	glGenVertexArrays(1, &ballVAO);
//	glGenBuffers(1, &ballVBO);
//	glBindVertexArray(ballVAO);
//	glGenBuffers(1, &ballEBO);
//
//	glBindBuffer(GL_ARRAY_BUFFER, ballVBO);
//	glBufferData(GL_ARRAY_BUFFER, ball.size()*sizeof(Vertex), ball.data(), GL_STATIC_DRAW);
//
//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ballEBO);
//	assignVAOstructure();

	/////////////////////////////
	// Ground creation
	//			world.Step(deltaTime*simulationSpeed/(float)stepRepeats, velocityIterations, positionIterations);

//	float groundHeight = 100.0f;
//	float groundWidth = 500.0f;
//
//	b2BodyDef groundBodyDef;
//	groundBodyDef.position.Set(0.0f, -groundHeight);
//
//	b2Body* groundBody = world.CreateBody(&groundBodyDef);
//	b2PolygonShape groundBox;
//	groundBox.SetAsBox(groundWidth, groundHeight);
//
//    b2FixtureDef groundFixtureDef;
//    groundFixtureDef.shape = &groundBox;
//    groundFixtureDef.restitution = 0.5f;
//    groundFixtureDef.friction = 0.5;
//    groundFixtureDef.density = 1.0f;
//	groundBody->CreateFixture(&groundFixtureDef);
//
//	std::vector<Vertex> ground = getGround(groundWidth, groundHeight, spritesheetWidth, spritesheetHeight);
//
//	unsigned int groundVAO, groundVBO, groundEBO;
//	glGenVertexArrays(1, &groundVAO);
//	glGenBuffers(1, &groundVBO);
//	glBindVertexArray(groundVAO);
//	glGenBuffers(1, &groundEBO);
//
//	glBindBuffer(GL_ARRAY_BUFFER, groundVBO);
//	glBufferData(GL_ARRAY_BUFFER, ground.size()*sizeof(Vertex), ground.data(), GL_STATIC_DRAW);
//
//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, groundEBO);
////	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(backgroundIndices), backgroundIndices, GL_STATIC_DRAW);
//	assignVAOstructure();

	/////////////////////////////
	// Camera setup
	/////////////////////////////

	Camera camera;
	camera.setMPosition(glm::vec3(0.0f, 0.0f, 30.0f));
	camera.setMPosition(glm::vec3(0.0f, 200.0f, 220.0f));
	camera.setMOrientation(glm::vec3(0.0f, 0.0f, -1.0f));
	camera.setMUp(glm::vec3(0.0f, 1.0f,  0.0f));

//	ballBodyDef.position.Set(0.8f, 50.0f);

	camera.setMSpeed(50.0f);
	camera.setMSensitivity(8.0f);
	camera.setMZoomSensitivity(10.0f);

	camera.setMYaw(-90.0f);
	camera.setMPitch(0.0f);

	camera.setMMinFov(10.0f);
	camera.setMMaxFov(120.0f);
	camera.setMCurFov(90.0f);

	/////////////////////////////
	// Runtime variables
	/////////////////////////////

	std::vector<unsigned int> frameCounter;
	bool isWireframe = false;
	bool isFullScreen = false;
	bool doPrintFPS = false;
	bool isAnimated = false;
	bool followBall = false;

	float deltaTime = 0.0f;	// Time between current frame and last frame
	float lastFrame = 0.0f; // Time of last frame

	float simulationSpeed = 1.0f;

	float simulationStep = 1.0f/60.0f;
	float remainingTime = 0.0f;

	int stepRepeats = 3.0f;

	//Main loop
	bool isRunning = true;
	while (isRunning)
	{
		unsigned int currentTick = SDL_GetTicks();

		float timePassed = (float)currentTick/1000.0f;
		deltaTime = timePassed - lastFrame;
		lastFrame = timePassed;

		std::cout<<"Time passed: "<<deltaTime<<std::endl;

		remainingTime += deltaTime;

//		world.Step(simulationStep, velocityIterations, positionIterations);

		while (remainingTime >= simulationStep)
		{
			remainingTime -= simulationStep;
		}

		for (int repeat=0; repeat<stepRepeats; repeat++)
		{
			world.Step(deltaTime*simulationSpeed/(float)stepRepeats, velocityIterations, positionIterations);
		}
//		world.Step(0.01f, velocityIterations, positionIterations);

//	    b2Vec2 position = dynamicBoxBody->GetPosition();
//	    float angle = dynamicBoxBody->GetAngle();
//	    printf("%4.2f %4.2f %4.2f\n", position.x, position.y, angle);

//	    float ball2Angle = ball2.m_physical->m_body->GetAngle();
//	    std::cout<<ball2Angle<<std::endl;

		frameCounter.push_back(currentTick);
		while (currentTick - frameCounter[0] >= 1000)
		{
			for (unsigned int iii=1; iii<frameCounter.size(); iii++)
			{
				frameCounter[iii-1] = frameCounter[iii];
			}
			frameCounter.pop_back();
		}

		if (doPrintFPS)
		{
			std::cout<<"Frames: "<<frameCounter.size()<<std::endl;
		}

	    camera.setMDeltaTime(deltaTime);
//		pegs[0].rotate(deltaTime);

//	    classPeg.rotate(deltaTime);

		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_KEYDOWN)
			{
				switch (event.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					isRunning = false;
					break;
				case 'p':
					isWireframe = !isWireframe;
					if (isWireframe)
					{
						glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
					}
					else
					{
						glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
					}
					break;
				case 'b':
				{
//					std::cout<<"######################"<<std::endl;
//					for (int coordinate=0; coordinate<ball2.m_physical->m_shape.m_count; coordinate++)
//					{
//						std::cout<<"Coordinate #"<<coordinate<<": ";
//						std::cout<<"x: "<<ball2.m_physical->m_shape.m_vertices[coordinate].x;
//						std::cout<<", y: "<<ball2.m_physical->m_shape.m_vertices[coordinate].y<<std::endl;
//					}

//					ball2.getMPosition().x * 1.0f;

//					glm::vec4 test(ball2.getMPosition());
//					float ballX_originalVariableName = ball2.getMPosition().x;
//					float ballY = ball2.getMPosition().y;
//					std::cout<<"x: "<<ballX_originalVariableName<<" y:"<<ballY<<std::endl;

					break;
				}
				case 'f':
					isFullScreen = !isFullScreen;
					if (isFullScreen)
					{
						SDL_SetWindowFullscreen(window, windowFlags | SDL_WINDOW_FULLSCREEN_DESKTOP);
						SDL_GetWindowSize(window, &winWidth, &winHeight);
						std::cout<<winWidth<<"x"<<winHeight<<std::endl;
					}
					else
					{
						SDL_SetWindowFullscreen(window, windowFlags);
					}
					break;
				default:
					//					std::cout<<"Key not implemented"<<std::endl;
					break;
				}
			}

			if (event.type == SDL_QUIT)
			{
				isRunning = false;
			}

			camera.handleMouseInput(&event);
		}

		camera.handleKeyboardInput();

		/////////////////////////////
		// Transformations
		/////////////////////////////

		camera.updateWidthAndHeight(winWidth, winHeight);
//		GLfloat xbruh = ball2.getMPosition().x;
//		float ybruh = ball2.getMPosition().y;

		if (followBall)
		{
//			camera.setMPosition(glm::vec3(ballBody->GetTransform().p.x, ballBody->GetTransform().p.y, 8.0f));

//			camera.setMPosition(glm::vec3(ball2.getMPosition().x, ball2.getMPosition().y, 8.0f));
		}

		int viewLoc = glGetUniformLocation(defaultShader.ID, "view");
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(camera.getView()));

		int projectionLoc = glGetUniformLocation(defaultShader.ID, "projection");
		glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(camera.getProjection()));

		/////////////////////////////
		// Animation
		/////////////////////////////

		glm::mat4 trans = glm::mat4(1.0f);

		if (isAnimated)
		{
			trans = glm::translate(trans, glm::vec3(0.0f, 0.0f, 0.0f));
			trans = glm::rotate(trans, glm::radians((float)(currentTick/10%360)), glm::vec3(0.2, 0.5, 1.0));
			trans = glm::rotate(trans, glm::radians((float)(currentTick/5%360)), glm::vec3(0.2, 0.5, 0.0));
			trans = glm::rotate(trans, glm::radians((float)(currentTick/8%360)), glm::vec3(0.8, 0.1, 0.6));
			trans = glm::scale(trans, glm::vec3(1.0f));
		}

		//Apply animation, will do nothing if animation is disabled
		unsigned int transformLoc = glGetUniformLocation(defaultShader.ID, "transform");
		glUniformMatrix4fv(transformLoc, 1, GL_FALSE, glm::value_ptr(trans));

		/////////////////////////////
		// Render
		/////////////////////////////

		//Clear the screen
		glClearColor(0.05f, 0.1f, 0.15f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Update viewport
		SDL_GetWindowSize(window, &winWidth, &winHeight);
		glViewport(0, 0, winWidth, winHeight);

		//Activate shader
		defaultShader.use();

		//Activate textures
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, spriteSheetTexture);

		/////////////////////////////
		// Model transformations and rendering
		/////////////////////////////

		/////////////////////////////
		// Pegs

		for (int iii=0; iii<pegField.size(); iii++)
		{
			pegField[iii]->draw(defaultShader);
		}

		/////////////////////////////
		// Balls

		for (int iii=0; iii<ballField.size(); iii++)
		{
			ballField[iii]->draw(defaultShader);
		}

		/////////////////////////////
		// Moving platforms

		for (int iii=0; iii<platforms.size(); iii++)
		{
			platforms[iii]->draw(defaultShader);
			platforms[iii]->act();
		}

		/////////////////////////////
		// Board

		board.draw(defaultShader);
		boardRoof.draw(defaultShader);

		/////////////////////////////
		// Draw everything
//
//		for (unsigned int entity = 0; entity<positions.size(); entity++)
//		{
//			model = glm::mat4(1.0f);
//			model = glm::translate(model, positions[entity]);
//			glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
//
//			glBindVertexArray(VAOs[entity]);
//			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, boxEBO);
//			glDrawArrays(GL_TRIANGLES, 0, sizes[entity]);
//		}

		/////////////////////////////
		// Finishing up
		/////////////////////////////

		//Output FPS in window title
		std::string newTitle = windowTitle+", FPS: "+std::to_string(frameCounter.size());
		SDL_SetWindowTitle(window, newTitle.c_str());

//		SDL_GL_SetSwapInterval(1);
		SDL_GL_SwapWindow(window);

	}

	SDL_DestroyWindow(window);
	SDL_GL_DeleteContext(context);
	return 0;
}
