#include "Mesh.h"

#include <algorithm>

#include <random>


Mesh::Mesh()
{

}

Mesh::Mesh(std::vector<Vertex>& vertices, std::vector<GLuint>& indices)
{
	m_vertices = vertices;
	m_indices = indices;

	//	m_position = glm::vec4(0.0f);

	m_vao.Bind();

	// Generates Vertex Buffer Object and links it to vertices
	VBO vbo(vertices);

	// Links VBO attributes such as coordinates and colors to VAO
	m_vao.LinkAttrib(vbo, 0, 3, GL_FLOAT, sizeof(Vertex), (void*) (0 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 1, 3, GL_FLOAT, sizeof(Vertex), (void*) (3 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 2, 3, GL_FLOAT, sizeof(Vertex), (void*) (6 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 3, 2, GL_FLOAT, sizeof(Vertex), (void*) (9 * sizeof(float)));

	// Unbind all to prevent accidentally modifying them
	m_vao.Unbind();

	//vbo.Delete();
	vbo.Unbind();
}

void Mesh::draw(b2Body* transform, Shader shader, float debugAngle)
{
	m_vao.Bind();

	glm::vec3 position;
	position.x = transform->GetPosition().x;
	position.y = transform->GetPosition().y;

	glm::mat4 model(1.0f);
	model = glm::translate(model, position);
	model = glm::rotate(model, transform->GetAngle()+debugAngle, glm::vec3(0.0f, 0.0f, 1.0f));

	int modelLoc = glGetUniformLocation(shader.ID, "model");
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

	// Draw the actual mesh
	glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT, 0);
	glDrawArrays(GL_TRIANGLES, 0, m_vertices.size());

	//	std::cout<<"drawing this"<<std::endl;
}

Mesh::~Mesh()
{
	//	std::cout << "Mesh destroyed" << std::endl;
	m_vao.Delete();
}

void Mesh::setToPeg(float radius, int spritesWidth, int spritesHeight)
{
	float radiusGraphical = radius;
	float radiusPhysical = radius;

	//Make a circle with this number of points on the perimeter
	int resolution = 32;
	GLfloat twicePi = 2.0f * glm::pi<float>();

	std::vector<glm::vec3> positions;
	std::vector<glm::vec2> textureCoordinates;

	float centerTexcoordX = 64*7+16;
	float centerTexcoordY = 48;

	//Start with the middle point
	positions.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
	textureCoordinates.push_back(glm::vec2(centerTexcoordX / spritesWidth, centerTexcoordY / spritesHeight));

	for (int step=0; step<resolution; step++)
	{
		float progress = (float)step/(float)resolution;
		progress *= twicePi;
		float x = cos(progress) * radiusGraphical;
		float y = sin(progress) * radiusGraphical;
		float tx = cos(progress) * 16 + centerTexcoordX;
		float ty = sin(progress) * 16 + centerTexcoordY;
		tx /= spritesWidth;
		ty /= spritesHeight;

		positions.push_back(glm::vec3(x, y, 0.0f));
		textureCoordinates.push_back(glm::vec2(tx, ty));
		m_physicalVertices.push_back({x, y});

		progress = ((float)(step+1))/(float)resolution;
		progress *= twicePi;
		x = cos(progress) * radiusGraphical;
		y = sin(progress) * radiusGraphical;
		tx = cos(progress) * 16 + centerTexcoordX;
		ty = sin(progress) * 16 + centerTexcoordY;
		tx /= spritesWidth;
		ty /= spritesHeight;

		positions.push_back(glm::vec3(x, y, 0.0f));
		textureCoordinates.push_back(glm::vec2(tx, ty));
	}

	std::vector<glm::vec3> colors;
	colors.push_back(glm::vec3(0.4f, 0.4f, 0.8f));
	colors[0] *= 0.5f;
	colors[0] = glm::vec3(0.8f);


	std::vector<glm::vec3> normals;
	normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));

	std::vector<Vertex> vertices;
	for (int iii=0; iii<resolution; iii++)
	{
		vertices.push_back({positions[0], colors[0], normals[0], textureCoordinates[0]});
		vertices.push_back({positions[2*iii+1], colors[0], normals[0], textureCoordinates[2*iii+1]});
		vertices.push_back({positions[2*iii+2], colors[0], normals[0], textureCoordinates[2*iii+2]});
	}

	m_circleShapes.push_back(std::shared_ptr<b2CircleShape>(new b2CircleShape));
	m_circleShapes[0]->m_radius = radiusPhysical;

	m_vertices = vertices;

	m_vao.Bind();

	// Generates Vertex Buffer Object and links it to vertices
	VBO vbo(m_vertices);

	// Links VBO attributes such as coordinates and colors to VAO
	m_vao.LinkAttrib(vbo, 0, 3, GL_FLOAT, sizeof(Vertex), (void*) (0 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 1, 3, GL_FLOAT, sizeof(Vertex), (void*) (3 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 2, 3, GL_FLOAT, sizeof(Vertex), (void*) (6 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 3, 2, GL_FLOAT, sizeof(Vertex), (void*) (9 * sizeof(float)));

	// Unbind all to prevent accidentally modifying them
	m_vao.Unbind();

	//vbo.Delete();
	vbo.Unbind();
}

//void Mesh::setToBall(float radius, int spritesWidth, int spritesHeight)
//{
//	//Make a circle with this number of points on the perimeter
//	int resolution = 32;
//	GLfloat twicePi = 2.0f * glm::pi<float>();
//
//	std::vector<glm::vec3> positions;
//	std::vector<glm::vec2> textureCoordinates;
//
//	float centerTexcoordX = 64*7+16;
//	float centerTexcoordY = 48;
//
//	//Start with the middle point
//	positions.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
//	textureCoordinates.push_back(glm::vec2(centerTexcoordX / spritesWidth, centerTexcoordY / spritesHeight));
//
//	for (int step=0; step<resolution; step++)
//	{
//		float progress = (float)step/(float)resolution;
//		progress *= twicePi;
//		float x = cos(progress) * radius;
//		float y = sin(progress) * radius;
//		float tx = cos(progress) * 16 + centerTexcoordX;
//		float ty = sin(progress) * 16 + centerTexcoordY;
//		tx /= spritesWidth;
//		ty /= spritesHeight;
//
//		positions.push_back(glm::vec3(x, y, 0.0f));
//		textureCoordinates.push_back(glm::vec2(tx, ty));
//		m_physicalVertices.push_back({x, y});
//
//		progress = ((float)(step+1))/(float)resolution;
//		progress *= twicePi;
//		x = cos(progress) * radius;
//		y = sin(progress) * radius;
//		tx = cos(progress) * 16 + centerTexcoordX;
//		ty = sin(progress) * 16 + centerTexcoordY;
//		tx /= spritesWidth;
//		ty /= spritesHeight;
//
//		positions.push_back(glm::vec3(x, y, 0.0f));
//		textureCoordinates.push_back(glm::vec2(tx, ty));
//	}
//
//	std::vector<glm::vec3> colors;
//	colors.push_back(glm::vec3(0.2f, 0.8f, 0.9f));
//	colors[0] *= 0.9f;
//
//	std::vector<glm::vec3> normals;
//	normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));
//
//	std::vector<Vertex> vertices;
//	for (int iii=0; iii<resolution; iii++)
//	{
//		vertices.push_back({positions[0], colors[0], normals[0], textureCoordinates[0]});
//		vertices.push_back({positions[2*iii+1], colors[0], normals[0], textureCoordinates[2*iii+1]});
//		vertices.push_back({positions[2*iii+2], colors[0], normals[0], textureCoordinates[2*iii+2]});
//	}
//
//	std::reverse(m_physicalVertices.begin(), m_physicalVertices.end());
//
////	for (int coordinate=0; coordinate<m_physicalVertices.size(); coordinate++)
////	{
////		std::cout<<"Coordinate #"<<coordinate<<": ";
////		std::cout<<"x: "<<m_physicalVertices[coordinate].x;
////		std::cout<<", y: "<<m_physicalVertices[coordinate].y<<std::endl;
////	}
//
//	m_vertices = vertices;
//
//	m_vao.Bind();
//
//	// Generates Vertex Buffer Object and links it to vertices
//	VBO vbo(m_vertices);
//
//	// Links VBO attributes such as coordinates and colors to VAO
//	m_vao.LinkAttrib(vbo, 0, 3, GL_FLOAT, sizeof(Vertex), (void*) (0 * sizeof(float)));
//	m_vao.LinkAttrib(vbo, 1, 3, GL_FLOAT, sizeof(Vertex), (void*) (3 * sizeof(float)));
//	m_vao.LinkAttrib(vbo, 2, 3, GL_FLOAT, sizeof(Vertex), (void*) (6 * sizeof(float)));
//	m_vao.LinkAttrib(vbo, 3, 2, GL_FLOAT, sizeof(Vertex), (void*) (9 * sizeof(float)));
//
//	// Unbind all to prevent accidentally modifying them
//	m_vao.Unbind();
//
////	vbo.Delete();
//	vbo.Unbind();
//}
//
//void Mesh::setToBall2(float radius, int spritesWidth, int spritesHeight)
//{
//	//Make a circle with this number of points on the perimeter
//	int resolution = 64;
//	assert(resolution>=6);
//	assert(resolution%2==0);
//
//	assert(resolution>=12);
//	assert(resolution%4==0);
//
//	GLfloat twicePi = 2.0f * glm::pi<float>();
//
//	std::vector<glm::vec3> positions;
//	std::vector<glm::vec2> textureCoordinates;
//
//	float centerTexcoordX = 64*7+16;
//	float centerTexcoordY = 48;
//
//	//Start with the middle point
//	positions.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
//	textureCoordinates.push_back(glm::vec2(centerTexcoordX / spritesWidth, centerTexcoordY / spritesHeight));
//
//	for (int step=0; step<resolution; step++)
//	{
//		float progress = (float)step/(float)resolution;
//		progress *= twicePi;
//		float x = cos(progress) * radius;
//		float y = sin(progress) * radius;
//		float tx = cos(progress) * 16 + centerTexcoordX;
//		float ty = sin(progress) * 16 + centerTexcoordY;
//		tx /= spritesWidth;
//		ty /= spritesHeight;
//
//		positions.push_back(glm::vec3(x, y, 0.0f));
//		textureCoordinates.push_back(glm::vec2(tx, ty));
//		m_physicalVertices.push_back({x, y});
//
//		progress = ((float)(step+1))/(float)resolution;
//		progress *= twicePi;
//		x = cos(progress) * radius;
//		y = sin(progress) * radius;
//		tx = cos(progress) * 16 + centerTexcoordX;
//		ty = sin(progress) * 16 + centerTexcoordY;
//		tx /= spritesWidth;
//		ty /= spritesHeight;
//
//		positions.push_back(glm::vec3(x, y, 0.0f));
//		textureCoordinates.push_back(glm::vec2(tx, ty));
//	}
//
//	std::vector<glm::vec3> colors;
//	colors.push_back(glm::vec3(0.2f, 0.8f, 0.9f));
//	colors[0] *= 0.9f;
//
//	std::vector<glm::vec3> normals;
//	normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));
//
//	std::vector<Vertex> vertices;
//	for (int iii=0; iii<resolution; iii++)
//	{
//		vertices.push_back({positions[0], colors[0], normals[0], textureCoordinates[0]});
//		vertices.push_back({positions[2*iii+1], colors[0], normals[0], textureCoordinates[2*iii+1]});
//		vertices.push_back({positions[2*iii+2], colors[0], normals[0], textureCoordinates[2*iii+2]});
//	}
//
////	std::reverse(m_physicalVertices.begin(), m_physicalVertices.end());
//
////	for (int coordinate=0; coordinate<m_physicalVertices.size(); coordinate++)
////	{
////		std::cout<<"Coordinate #"<<coordinate<<": ";
////		std::cout<<"x: "<<m_physicalVertices[coordinate].x;
////		std::cout<<", y: "<<m_physicalVertices[coordinate].y<<std::endl;
////	}
//
//	m_vertices = vertices;
//
//	std::vector<b2PolygonShape> boxes;
//
//	int halfResolution = resolution/2;
//	//Create boxes across the ball to form the approximation of the ball
////	for (int vertex=0; vertex<halfResolution; vertex++)
//	for (int vertex=0; vertex<halfResolution; vertex += 3)
//	{
//		float x, y, index;
//		std::vector<b2Vec2> points;
//
//		b2Vec2 newPoints[8];
//
//		index = vertex+0;
//		newPoints[0].x = m_physicalVertices[index].x;
//		newPoints[0].y = m_physicalVertices[index].y;
//
//		index = vertex+1;
//		newPoints[1].x = m_physicalVertices[index].x;
//		newPoints[1].y = m_physicalVertices[index].y;
//
//		index = vertex+2;
//		newPoints[2].x = m_physicalVertices[index].x;
//		newPoints[2].y = m_physicalVertices[index].y;
//
//		index = vertex+3;
//		newPoints[3].x = m_physicalVertices[index].x;
//		newPoints[3].y = m_physicalVertices[index].y;
//
//		index = (vertex+halfResolution+0)%resolution;
//		newPoints[4].x = m_physicalVertices[index].x;
//		newPoints[4].y = m_physicalVertices[index].y;
//
//		index = (vertex+halfResolution+1)%resolution;
//		newPoints[5].x = m_physicalVertices[index].x;
//		newPoints[5].y = m_physicalVertices[index].y;
//
//		index = (vertex+halfResolution+2)%resolution;
//		newPoints[6].x = m_physicalVertices[index].x;
//		newPoints[6].y = m_physicalVertices[index].y;
//
//		index = (vertex+halfResolution+3)%resolution;
//		newPoints[7].x = m_physicalVertices[index].x;
//		newPoints[7].y = m_physicalVertices[index].y;
//
//		x = m_physicalVertices[vertex].x;
//		y = m_physicalVertices[vertex].y;
//		points.push_back({x, y});
//
//		x = m_physicalVertices[vertex+1].x;
//		y = m_physicalVertices[vertex+1].y;
//		points.push_back({x, y});
//
//		x = m_physicalVertices[vertex+halfResolution-1].x;
//		y = m_physicalVertices[vertex+halfResolution-1].y;
//		points.push_back({x, y});
//
//		x = m_physicalVertices[vertex+halfResolution].x;
//		y = m_physicalVertices[vertex+halfResolution].y;
//		points.push_back({x, y});
//
////		std::reverse(points.begin(), points.end());
//
//		b2PolygonShape newBox;
////		newBox.Set(points.data(), 4);
//		newBox.Set(newPoints, 8);
////		newBox.m_centroid = b2Vec2(0.0f, 0.0f);
//
//		boxes.push_back(newBox);
//
////		m_boxes.push_back(static_cast<std::shared_ptr<b2PolygonShape>>(new b2PolygonShape()));
////		m_boxes.back()->Se
//	}
//
//	m_boxes = boxes;
//	std::cout<<"Boxes size: "<<m_boxes.size()<<std::endl;
//
//	m_vao.Bind();
//
//	// Generates Vertex Buffer Object and links it to vertices
//	VBO vbo(m_vertices);
//
//	// Links VBO attributes such as coordinates and colors to VAO
//	m_vao.LinkAttrib(vbo, 0, 3, GL_FLOAT, sizeof(Vertex), (void*) (0 * sizeof(float)));
//	m_vao.LinkAttrib(vbo, 1, 3, GL_FLOAT, sizeof(Vertex), (void*) (3 * sizeof(float)));
//	m_vao.LinkAttrib(vbo, 2, 3, GL_FLOAT, sizeof(Vertex), (void*) (6 * sizeof(float)));
//	m_vao.LinkAttrib(vbo, 3, 2, GL_FLOAT, sizeof(Vertex), (void*) (9 * sizeof(float)));
//
//	// Unbind all to prevent accidentally modifying them
//	m_vao.Unbind();
//
////	vbo.Delete();
//	vbo.Unbind();
//}

void Mesh::setToBall3(float radius, int spritesWidth, int spritesHeight)
{
	float radiusGraphical = radius;
	float radiusPhysical = radius;

	//Make a circle with this number of points on the perimeter
	int resolution = 64;
	assert(resolution>=6);
	assert(resolution%2==0);

	assert(resolution>=12);
	assert(resolution%4==0);

	GLfloat twicePi = 2.0f * glm::pi<float>();

	std::vector<glm::vec3> positions;
	std::vector<glm::vec2> textureCoordinates;

	float centerTexcoordX = 64*7+16;
	float centerTexcoordY = 48;

	//Start with the middle point
	positions.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
	textureCoordinates.push_back(glm::vec2(centerTexcoordX / spritesWidth, centerTexcoordY / spritesHeight));

	for (int step=0; step<resolution; step++)
	{
		float progress = (float)step/(float)resolution;
		progress *= twicePi;
		float x = cos(progress) * radiusGraphical;
		float y = sin(progress) * radiusGraphical;
		float tx = cos(progress) * 16 + centerTexcoordX;
		float ty = sin(progress) * 16 + centerTexcoordY;
		tx /= spritesWidth;
		ty /= spritesHeight;

		positions.push_back(glm::vec3(x, y, 0.0f));
		textureCoordinates.push_back(glm::vec2(tx, ty));
		m_physicalVertices.push_back({x, y});

		progress = ((float)(step+1))/(float)resolution;
		progress *= twicePi;
		x = cos(progress) * radiusGraphical;
		y = sin(progress) * radiusGraphical;
		tx = cos(progress) * 16 + centerTexcoordX;
		ty = sin(progress) * 16 + centerTexcoordY;
		tx /= spritesWidth;
		ty /= spritesHeight;

		positions.push_back(glm::vec3(x, y, 0.0f));
		textureCoordinates.push_back(glm::vec2(tx, ty));
	}

	std::vector<glm::vec3> colors;
	colors.push_back(glm::vec3(0.2f, 0.8f, 0.9f));
	colors[0] *= 0.9f;
	colors[0] = getRandomColor();

	std::vector<glm::vec3> normals;
	normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));

	std::vector<Vertex> vertices;
	for (int iii=0; iii<resolution; iii++)
	{
		vertices.push_back({positions[0], colors[0], normals[0], textureCoordinates[0]});
		vertices.push_back({positions[2*iii+1], colors[0], normals[0], textureCoordinates[2*iii+1]});
		vertices.push_back({positions[2*iii+2], colors[0], normals[0], textureCoordinates[2*iii+2]});
	}

	m_circleShapes.push_back(std::shared_ptr<b2CircleShape>(new b2CircleShape));
	m_circleShapes[0]->m_radius = radiusPhysical;

	m_vertices = vertices;

	m_vao.Bind();

	// Generates Vertex Buffer Object and links it to vertices
	VBO vbo(m_vertices);

	// Links VBO attributes such as coordinates and colors to VAO
	m_vao.LinkAttrib(vbo, 0, 3, GL_FLOAT, sizeof(Vertex), (void*) (0 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 1, 3, GL_FLOAT, sizeof(Vertex), (void*) (3 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 2, 3, GL_FLOAT, sizeof(Vertex), (void*) (6 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 3, 2, GL_FLOAT, sizeof(Vertex), (void*) (9 * sizeof(float)));

	// Unbind all to prevent accidentally modifying them
	m_vao.Unbind();

	//	vbo.Delete();
	vbo.Unbind();
}

void Mesh::setToBoard(float scale, int spritesWidth, int spritesHeight)
{
	float scaleGraphical = scale;
	float scalePhysical = scale;

	std::vector<b2Vec2> outer;
	std::vector<b2Vec2> inner;

	/////////////////////////////
	// Determine the outer segment

	//Bottom left
	outer.push_back({-100.0f, -200.0f});

	//Bottom right
	outer.push_back({100.0f, -200.0f});

	//Top left
	outer.push_back({100.0f, 200.0f});
	outer.push_back({90.0f, 200.0f});

	//Bottom of elevator
	outer.push_back({90.0f, -190.0f});
	outer.push_back({50.0f, -190.0f});

	//Left of shaft
	outer.push_back({-35.0f, -105.0f});
	outer.push_back({-35.0f, -85.0f});

	//Top of left hill
	outer.push_back({-90.0f, -30.0f});

	//Top left
	outer.push_back({-90.0f, 200.0f});
	outer.push_back({-100.0f, 200.0f});

	/////////////////////////////
	// Determine the inner segment

	//Elevator entrance
	inner.push_back({60.0f, -160.0f});

	//Elevator exit
	inner.push_back({60.0f, 200.0f});
	inner.push_back({50.0f, 200.0f});

	//Top of right hill
	inner.push_back({50.0f, -30.0f});

	//Right side of shaft
	inner.push_back({-5.0f, -85.0f});
	inner.push_back({-5.0f, -95.0f});


	std::vector<b2Vec2> renderOuter = outer;
	std::vector<b2Vec2> renderInner = inner;

	for (int iii=0; iii<outer.size(); iii++)
	{
		//		renderOuter.push_back(outer[iii]);
	}

	renderOuter.push_back({-100.0f, -190.0f});
	renderOuter.push_back({-90.0f, -190.0f});
	renderOuter.push_back({-35.0f, -190.0f});
	renderOuter.push_back({100.0f, -190.0f});
	renderOuter.push_back({-90.0f, -85.0f});

	for (int iii=0; iii<outer.size(); iii++)
	{
		//		renderInner.push_back(inner[iii]);
	}

	renderInner.push_back({60.0f, -95.0f});
	renderInner.push_back({60.0f, -85.0f});
	renderInner.push_back({50.0f, -85.0f});

	std::vector<glm::vec3> positions;
	std::vector<glm::vec2> textureCoordinates;

	float texcoordX = 64*7+16;
	float texcoordY = 48;

	for (unsigned int iii=0; iii<outer.size(); iii++)
	{
		float x = outer[iii].x;
		float y = outer[iii].y;
		float z = 0.0f;

		positions.push_back({x, y, z});
		textureCoordinates.push_back({x, y});
	}

	for (unsigned int iii=0; iii<inner.size(); iii++)
	{
		float x = inner[iii].x;
		float y = inner[iii].y;
		float z = 0.0f;

		positions.push_back({x, y, z});
		textureCoordinates.push_back({x, y});
	}

	std::vector<glm::vec3> colors;
	colors.push_back(glm::vec3(0.6f, 0.4f, 0.1f));
	colors[0] *= 0.8f;

	std::vector<glm::vec3> normals;
	normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));

	int a, b, c, d;

	std::vector<glm::vec3> outerTriangleSetup;
	//Bottom
	a=0;
	b=1;
	c=11;
	d=14;
	outerTriangleSetup.push_back({a, b, c});
	outerTriangleSetup.push_back({b, c, d});

	//Left wall
	a=11;
	b=12;
	c=10;
	d=9;
	outerTriangleSetup.push_back({a, b, c});
	outerTriangleSetup.push_back({b, c, d});

	//Right wall
	a=4;
	b=14;
	c=3;
	d=2;
	outerTriangleSetup.push_back({a, b, c});
	outerTriangleSetup.push_back({b, c, d});

	//Left filler
	a=12;
	b=13;
	c=15;
	d=7;
	outerTriangleSetup.push_back({a, b, c});
	outerTriangleSetup.push_back({b, c, d});

	//Left ramp
	a=15;
	b=7;
	c=8;
	outerTriangleSetup.push_back({a, b, c});

	//Elevator ramp
	a=13;
	b=5;
	c=6;
	outerTriangleSetup.push_back({a, b, c});

	std::vector<glm::vec3> innerTriangleSetup;
	//Elevator wall
	a=8;
	b=7;
	c=2;
	d=1;
	innerTriangleSetup.push_back({a, b, c});
	innerTriangleSetup.push_back({b, c, d});

	//Right ramp
	a=4;
	b=8;
	c=3;
	innerTriangleSetup.push_back({a, b, c});

	//Elevator shaft roof
	a=5;
	b=0;
	c=6;
	innerTriangleSetup.push_back({a, b, c});

	//Elevator middlebar
	a=5;
	b=6;
	c=4;
	d=7;
	innerTriangleSetup.push_back({a, b, c});
	innerTriangleSetup.push_back({b, c, d});

	std::vector<Vertex> vertices;

	for (int iii=0; iii<outerTriangleSetup.size(); iii++)
	{
		float a, b, c;
		a = outerTriangleSetup[iii].x;
		b = outerTriangleSetup[iii].y;
		c = outerTriangleSetup[iii].z;

		float x, y, z;
		z = 0.0f;

		x = renderOuter[a].x;
		y = renderOuter[a].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});

		x = renderOuter[b].x;
		y = renderOuter[b].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});

		x = renderOuter[c].x;
		y = renderOuter[c].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});
	}

	for (int iii=0; iii<innerTriangleSetup.size(); iii++)
	{
		float a, b, c;
		a = innerTriangleSetup[iii].x;
		b = innerTriangleSetup[iii].y;
		c = innerTriangleSetup[iii].z;

		float x, y, z;
		z = 0.0f;

		x = renderInner[a].x;
		y = renderInner[a].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});

		x = renderInner[b].x;
		y = renderInner[b].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});

		x = renderInner[c].x;
		y = renderInner[c].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});
	}

	m_chainShapes.push_back(std::shared_ptr<b2ChainShape>(new b2ChainShape));
	m_chainShapes.back()->CreateLoop(outer.data(), outer.size());

	m_chainShapes.push_back(std::shared_ptr<b2ChainShape>(new b2ChainShape));
	m_chainShapes.back()->CreateLoop(inner.data(), inner.size());

	m_vertices = vertices;

	m_vao.Bind();

	// Generates Vertex Buffer Object and links it to vertices
	VBO vbo(m_vertices);

	// Links VBO attributes such as coordinates and colors to VAO
	m_vao.LinkAttrib(vbo, 0, 3, GL_FLOAT, sizeof(Vertex), (void*) (0 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 1, 3, GL_FLOAT, sizeof(Vertex), (void*) (3 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 2, 3, GL_FLOAT, sizeof(Vertex), (void*) (6 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 3, 2, GL_FLOAT, sizeof(Vertex), (void*) (9 * sizeof(float)));

	// Unbind all to prevent accidentally modifying them
	m_vao.Unbind();

	//	vbo.Delete();
	vbo.Unbind();
}

void Mesh::setToBoardRoof(float scale, int spritesWidth, int spritesHeight)
{
	float scaleGraphical = scale;
	float scalePhysical = scale;

	std::vector<b2Vec2> outer;
	std::vector<b2Vec2> inner;
	std::vector<b2Vec2> splitter;

	/////////////////////////////
	// Determine the outer segment

	//Bottom left
	outer.push_back({-100.0f, -100.0f});

	//Left chute
	outer.push_back({-50.0f, -100.0f});

	//Left chute wall
	//	outer.push_back({-15.0f, -65.0f});
	outer.push_back({-5.0f, -65.0f});
	outer.push_back({-15.0f, -35.0f});

	//Top left inner
	outer.push_back({-90.0f, 40.0f});
	outer.push_back({-90.0f, 90.0f});

	//Top right inner
	outer.push_back({10.0f, 90.0f});
	outer.push_back({90.0f, 10.0f});

	//Bottom right
	outer.push_back({90.0f, -100.0f});
	outer.push_back({100.0f, -100.0f});

	//Top right
	outer.push_back({100.0f, 100.0f});

	//Top left
	outer.push_back({-100.0f, 100.0f});

	/////////////////////////////
	// Determine the inner segment

	//Bottom
	inner.push_back({50.0f, -100.0f});
	inner.push_back({60.0f, -100.0f});

	//Top right ramp
	inner.push_back({60.0f, 0.0f});
	inner.push_back({0.0f, 60.0f});

	//Top left corner
	inner.push_back({-60.0f, 60.0f});
	inner.push_back({-60.0f, 50.0f});

	//Right chute wall
	inner.push_back({15.0f, -25.0f});
	inner.push_back({5.0f, -65.0f});
	//	inner.push_back({15.0f, -65.0f});

	/////////////////////////////
	// Determine the splitter

	//Left
	splitter.push_back({-10.0f, -100.0f});

	//Right
	splitter.push_back({10.0f, -100.0f});

	//Top
	splitter.push_back({0.0f, -90.0f});

	/////////////////////////////
	// Determine the renderpath

	std::vector<b2Vec2> renderOuter = outer;
	renderOuter.push_back({-90.0f, -65.0f});
	renderOuter.push_back({-50.0f, -65.0f});
	renderOuter.push_back({-90.0f, -35.0f});
	renderOuter.push_back({-90.0f, 100.0f});
	renderOuter.push_back({-90.0f, -100.0f});
	renderOuter.push_back({90.0f, 90.0f});
	renderOuter.push_back({90.0f, 100.0f});

	std::vector<b2Vec2> renderInner = inner;
	std::cout<<"Render size: "<<renderInner.size()<<std::endl;
	renderInner.push_back({50.0f, -65.0f});
	renderInner.push_back({50.0f, -25.0f});
	renderInner.push_back({50.0f, 0.0f});
	renderInner.push_back({50.0f, 10.0f});
	renderInner.push_back({15.0f, 10.0f});
	renderInner.push_back({15.0f, 45.0f});
	renderInner.push_back({0.0f, 45.0f});
	renderInner.push_back({-55.0f, 45.0f});
	renderInner.push_back({-55.0f, 50.0f});
	renderInner.push_back({-55.0f, 60.0f});

	std::vector<b2Vec2> renderSplitter = splitter;

	std::vector<glm::vec3> positions;
	std::vector<glm::vec2> textureCoordinates;

	float texcoordX = 64*7+16;
	float texcoordY = 48;

	for (unsigned int iii=0; iii<outer.size(); iii++)
	{
		float x = outer[iii].x;
		float y = outer[iii].y;
		float z = 0.0f;

		positions.push_back({x, y, z});
		textureCoordinates.push_back({x, y});
	}

	for (unsigned int iii=0; iii<inner.size(); iii++)
	{
		float x = inner[iii].x;
		float y = inner[iii].y;
		float z = 0.0f;

		positions.push_back({x, y, z});
		textureCoordinates.push_back({x, y});
	}

	for (unsigned int iii=0; iii<splitter.size(); iii++)
	{
		float x = splitter[iii].x;
		float y = splitter[iii].y;
		float z = 0.0f;

		positions.push_back({x, y, z});
		textureCoordinates.push_back({x, y});
	}

	std::vector<glm::vec3> colors;
	colors.push_back(glm::vec3(0.6f, 0.4f, 0.1f));
	colors[0] *= 0.8f;

	std::vector<glm::vec3> normals;
	normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));

	int a, b, c, d;

	std::vector<glm::vec3> outerTriangleSetup;
	//Left wall
	a=0;
	b=16;
	c=11;
	d=15;
	outerTriangleSetup.push_back({a, b, c});
	outerTriangleSetup.push_back({b, c, d});

	//Bottomleft filler
	a=16;
	b=1;
	c=12;
	d=13;
	outerTriangleSetup.push_back({a, b, c});
	outerTriangleSetup.push_back({b, c, d});

	//Bottomleft middle filler
	a=12;
	b=2;
	c=14;
	d=3;
	outerTriangleSetup.push_back({a, b, c});
	outerTriangleSetup.push_back({b, c, d});

	//Left chute wall exit
	a=1;
	b=2;
	c=13;
	outerTriangleSetup.push_back({a, b, c});

	//Left exit ramp
	a=14;
	b=3;
	c=4;
	outerTriangleSetup.push_back({a, b, c});

	//Top
	a=5;
	b=17;
	c=15;
	d=18;
	outerTriangleSetup.push_back({a, b, c});
	outerTriangleSetup.push_back({b, c, d});

	//Right wall
	a=8;
	b=9;
	c=18;
	d=10;
	outerTriangleSetup.push_back({a, b, c});
	outerTriangleSetup.push_back({b, c, d});

	//Right roof angle
	a=6;
	b=7;
	c=17;
	outerTriangleSetup.push_back({a, b, c});

	std::vector<glm::vec3> innerTriangleSetup;
	//Right wall
	a=0;
	b=1;
	c=10;
	d=2;
	innerTriangleSetup.push_back({a, b, c});
	innerTriangleSetup.push_back({b, c, d});

	//Right chute wall exit
	a=7;
	b=0;
	c=8;
	innerTriangleSetup.push_back({a, b, c});

	//Lower middle filler
	a=7;
	b=8;
	c=6;
	d=9;
	innerTriangleSetup.push_back({a, b, c});
	innerTriangleSetup.push_back({b, c, d});

	//Upper middle filler
	a=6;
	b=9;
	c=12;
	d=11;
	innerTriangleSetup.push_back({a, b, c});
	innerTriangleSetup.push_back({b, c, d});

	//Right ramp bottom
	a=10;
	b=2;
	c=11;
	innerTriangleSetup.push_back({a, b, c});

	//right ramp middle
	a=12;
	b=11;
	c=13;
	innerTriangleSetup.push_back({a, b, c});

	//right ramp top
	a=14;
	b=13;
	c=3;
	innerTriangleSetup.push_back({a, b, c});

	//Chute downramp right angle bottom
	a=15;
	b=6;
	c=13;
	innerTriangleSetup.push_back({a, b, c});

	//Chute downramp right angle top
	a=5;
	b=15;
	c=16;
	innerTriangleSetup.push_back({a, b, c});

	//Top left
	a=5;
	b=16;
	c=4;
	d=17;
	innerTriangleSetup.push_back({a, b, c});
	innerTriangleSetup.push_back({b, c, d});

	//Top
	a=15;
	b=14;
	c=17;
	d=3;
	innerTriangleSetup.push_back({a, b, c});
	innerTriangleSetup.push_back({b, c, d});

	std::vector<glm::vec3> splitterTriangleSetup;
	a=0;
	b=1;
	c=2;
	splitterTriangleSetup.push_back({a, b, c});

	std::vector<Vertex> vertices;

	for (int iii=0; iii<outerTriangleSetup.size(); iii++)
	{
		float a, b, c;
		a = outerTriangleSetup[iii].x;
		b = outerTriangleSetup[iii].y;
		c = outerTriangleSetup[iii].z;

		float x, y, z;
		z = 0.0f;

		x = renderOuter[a].x;
		y = renderOuter[a].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});

		x = renderOuter[b].x;
		y = renderOuter[b].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});

		x = renderOuter[c].x;
		y = renderOuter[c].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});
	}

	for (int iii=0; iii<innerTriangleSetup.size(); iii++)
	{
		float a, b, c;
		a = innerTriangleSetup[iii].x;
		b = innerTriangleSetup[iii].y;
		c = innerTriangleSetup[iii].z;

		float x, y, z;
		z = 0.0f;

		x = renderInner[a].x;
		y = renderInner[a].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});

		x = renderInner[b].x;
		y = renderInner[b].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});

		x = renderInner[c].x;
		y = renderInner[c].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});
	}

	for (int iii=0; iii<splitterTriangleSetup.size(); iii++)
	{
		float a, b, c;
		a = splitterTriangleSetup[iii].x;
		b = splitterTriangleSetup[iii].y;
		c = splitterTriangleSetup[iii].z;

		float x, y, z;
		z = 0.0f;

		x = renderSplitter[a].x;
		y = renderSplitter[a].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});

		x = renderSplitter[b].x;
		y = renderSplitter[b].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});

		x = renderSplitter[c].x;
		y = renderSplitter[c].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});
	}

	m_chainShapes.push_back(std::shared_ptr<b2ChainShape>(new b2ChainShape));
	m_chainShapes.back()->CreateLoop(outer.data(), outer.size());

	m_chainShapes.push_back(std::shared_ptr<b2ChainShape>(new b2ChainShape));
	m_chainShapes.back()->CreateLoop(inner.data(), inner.size());

	m_chainShapes.push_back(std::shared_ptr<b2ChainShape>(new b2ChainShape));
	m_chainShapes.back()->CreateLoop(splitter.data(), splitter.size());

	m_vertices = vertices;

	m_vao.Bind();

	// Generates Vertex Buffer Object and links it to vertices
	VBO vbo(m_vertices);

	// Links VBO attributes such as coordinates and colors to VAO
	m_vao.LinkAttrib(vbo, 0, 3, GL_FLOAT, sizeof(Vertex), (void*) (0 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 1, 3, GL_FLOAT, sizeof(Vertex), (void*) (3 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 2, 3, GL_FLOAT, sizeof(Vertex), (void*) (6 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 3, 2, GL_FLOAT, sizeof(Vertex), (void*) (9 * sizeof(float)));

	// Unbind all to prevent accidentally modifying them
	m_vao.Unbind();

	//	vbo.Delete();
	vbo.Unbind();
}

void Mesh::setToBoardRoof2(float scale, int spritesWidth, int spritesHeight)
{
	float scaleGraphical = scale;
	float scalePhysical = scale;

	std::vector<b2Vec2> outer;
	std::vector<b2Vec2> inner;
	std::vector<b2Vec2> splitter;

	/////////////////////////////
	// Determine the outer segment

	//Bottom left
	outer.push_back({-100.0f, -100.0f});
	outer.push_back({-80.0f, -100.0f});

	//Middle platform
	outer.push_back({-10.0f, -30.0f});
	outer.push_back({10.0f, -30.0f});
	outer.push_back({20.0f, -20.0f});
	outer.push_back({20.0f, -10.0f});
	outer.push_back({10.0f, 0.0f});

	//Left C pipe
	outer.push_back({-60.0f, 0.0f});
	outer.push_back({-90.0f, 30.0f});
	outer.push_back({-90.0f, 60.0f});
	outer.push_back({-60.0f, 90.0f});

	//Right roof angle
	//	outer.push_back({60.0f, 90.0f});
	outer.push_back({60.0f, 80.0f});
	outer.push_back({90.0f, 60.0f});

	//Bottom right
	outer.push_back({90.0f, -100.0f});
	outer.push_back({100.0f, -100.0f});

	//Top right
	outer.push_back({100.0f, 100.0f});

	//Top left
	outer.push_back({-100.0f, 100.0f});

	/////////////////////////////
	// Determine the inner segment

	//Bottom
	inner.push_back({40.0f, -100.0f});
	inner.push_back({60.0f, -100.0f});

	//Top right ramp
	inner.push_back({60.0f, 50.0f});
	inner.push_back({50.0f, 60.0f});

	//Left edge
	inner.push_back({-50.0f, 60.0f});
	inner.push_back({-60.0f, 50.0f});
	inner.push_back({-60.0f, 40.0f});
	inner.push_back({-50.0f, 30.0f});

	//Right C pipe
	inner.push_back({20.0f, 30.0f});
	inner.push_back({50.0f, 0.0f});
	inner.push_back({50.0f, -30.0f});
	inner.push_back({20.0f, -60.0f});

	//Exit platform
	inner.push_back({10.0f, -60.0f});

	/////////////////////////////
	// Determine the splitter

	//Left
	splitter.push_back({-40.0f, -100.0f});

	//Right
	splitter.push_back({0.0f, -100.0f});

	//Top
	splitter.push_back({-20.0f, -80.0f});

	/////////////////////////////
	// Determine the renderpath

	std::vector<b2Vec2> renderOuter = outer;
	//	renderOuter.push_back({-90.0f, -65.0f});
	//	renderOuter.push_back({-50.0f, -65.0f});

	std::vector<b2Vec2> renderInner = inner;
	std::cout<<"Render size: "<<renderInner.size()<<std::endl;
	//	renderInner.push_back({50.0f, -65.0f});
	//	renderInner.push_back({50.0f, -25.0f});

	std::vector<b2Vec2> renderSplitter = splitter;

	std::vector<glm::vec3> positions;
	std::vector<glm::vec2> textureCoordinates;

	float texcoordX = 64*7+16;
	float texcoordY = 48;

	for (unsigned int iii=0; iii<outer.size(); iii++)
	{
		float x = outer[iii].x;
		float y = outer[iii].y;
		float z = 0.0f;

		positions.push_back({x, y, z});
		textureCoordinates.push_back({x, y});
	}

	for (unsigned int iii=0; iii<inner.size(); iii++)
	{
		float x = inner[iii].x;
		float y = inner[iii].y;
		float z = 0.0f;

		positions.push_back({x, y, z});
		textureCoordinates.push_back({x, y});
	}

	for (unsigned int iii=0; iii<splitter.size(); iii++)
	{
		float x = splitter[iii].x;
		float y = splitter[iii].y;
		float z = 0.0f;

		positions.push_back({x, y, z});
		textureCoordinates.push_back({x, y});
	}

	std::vector<glm::vec3> colors;
	colors.push_back(glm::vec3(0.6f, 0.4f, 0.1f));
	colors[0] *= 0.8f;

	std::vector<glm::vec3> normals;
	normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));

	int a, b, c, d;

	std::vector<glm::vec3> outerTriangleSetup;
	a=0;
	b=1;
	c=2;
	outerTriangleSetup.push_back({a, b, c});

	a=0;
	b=2;
	c=7;
	outerTriangleSetup.push_back({a, b, c});

	a=2;
	b=3;
	c=4;
	outerTriangleSetup.push_back({a, b, c});

	a=2;
	b=4;
	c=5;
	outerTriangleSetup.push_back({a, b, c});

	a=2;
	b=5;
	c=6;
	outerTriangleSetup.push_back({a, b, c});

	a=2;
	b=6;
	c=7;
	outerTriangleSetup.push_back({a, b, c});

	a=0;
	b=7;
	c=8;
	outerTriangleSetup.push_back({a, b, c});

	a=0;
	b=8;
	c=9;
	outerTriangleSetup.push_back({a, b, c});

	a=0;
	b=9;
	c=16;
	outerTriangleSetup.push_back({a, b, c});

	a=9;
	b=10;
	c=16;
	outerTriangleSetup.push_back({a, b, c});

	a=16;
	b=10;
	c=15;
	outerTriangleSetup.push_back({a, b, c});

	a=10;
	b=11;
	c=15;
	outerTriangleSetup.push_back({a, b, c});

	a=11;
	b=12;
	c=15;
	outerTriangleSetup.push_back({a, b, c});

	a=12;
	b=14;
	c=15;
	outerTriangleSetup.push_back({a, b, c});

	a=13;
	b=14;
	c=12;
	outerTriangleSetup.push_back({a, b, c});

	std::vector<glm::vec3> innerTriangleSetup;
	a=12;
	b=0;
	c=1;
	innerTriangleSetup.push_back({a, b, c});

	a=12;
	b=1;
	c=11;
	innerTriangleSetup.push_back({a, b, c});

	a=11;
	b=1;
	c=10;
	innerTriangleSetup.push_back({a, b, c});

	a=10;
	b=1;
	c=2;
	innerTriangleSetup.push_back({a, b, c});

	a=10;
	b=2;
	c=9;
	innerTriangleSetup.push_back({a, b, c});

	a=9;
	b=2;
	c=3;
	innerTriangleSetup.push_back({a, b, c});

	a=8;
	b=9;
	c=2;
	innerTriangleSetup.push_back({a, b, c});

	a=8;
	b=2;
	c=3;
	innerTriangleSetup.push_back({a, b, c});

	a=8;
	b=3;
	c=4;
	innerTriangleSetup.push_back({a, b, c});

	a=8;
	b=4;
	c=5;
	innerTriangleSetup.push_back({a, b, c});

	a=8;
	b=5;
	c=6;
	innerTriangleSetup.push_back({a, b, c});

	a=8;
	b=6;
	c=7;
	innerTriangleSetup.push_back({a, b, c});


	std::vector<glm::vec3> splitterTriangleSetup;
	a=0;
	b=1;
	c=2;
	splitterTriangleSetup.push_back({a, b, c});

	std::vector<Vertex> vertices;

	for (int iii=0; iii<outerTriangleSetup.size(); iii++)
	{
		float a, b, c;
		a = outerTriangleSetup[iii].x;
		b = outerTriangleSetup[iii].y;
		c = outerTriangleSetup[iii].z;

		float x, y, z;
		z = 0.0f;

		x = renderOuter[a].x;
		y = renderOuter[a].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});

		x = renderOuter[b].x;
		y = renderOuter[b].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});

		x = renderOuter[c].x;
		y = renderOuter[c].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});
	}

	for (int iii=0; iii<innerTriangleSetup.size(); iii++)
	{
		float a, b, c;
		a = innerTriangleSetup[iii].x;
		b = innerTriangleSetup[iii].y;
		c = innerTriangleSetup[iii].z;

		float x, y, z;
		z = 0.0f;

		x = renderInner[a].x;
		y = renderInner[a].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});

		x = renderInner[b].x;
		y = renderInner[b].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});

		x = renderInner[c].x;
		y = renderInner[c].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});
	}

	for (int iii=0; iii<splitterTriangleSetup.size(); iii++)
	{
		float a, b, c;
		a = splitterTriangleSetup[iii].x;
		b = splitterTriangleSetup[iii].y;
		c = splitterTriangleSetup[iii].z;

		float x, y, z;
		z = 0.0f;

		x = renderSplitter[a].x;
		y = renderSplitter[a].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});

		x = renderSplitter[b].x;
		y = renderSplitter[b].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});

		x = renderSplitter[c].x;
		y = renderSplitter[c].y;
		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});
	}

	m_chainShapes.push_back(std::shared_ptr<b2ChainShape>(new b2ChainShape));
	m_chainShapes.back()->CreateLoop(outer.data(), outer.size());

	m_chainShapes.push_back(std::shared_ptr<b2ChainShape>(new b2ChainShape));
	m_chainShapes.back()->CreateLoop(inner.data(), inner.size());

	m_chainShapes.push_back(std::shared_ptr<b2ChainShape>(new b2ChainShape));
	m_chainShapes.back()->CreateLoop(splitter.data(), splitter.size());

	m_vertices = vertices;

	m_vao.Bind();

	// Generates Vertex Buffer Object and links it to vertices
	VBO vbo(m_vertices);

	// Links VBO attributes such as coordinates and colors to VAO
	m_vao.LinkAttrib(vbo, 0, 3, GL_FLOAT, sizeof(Vertex), (void*) (0 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 1, 3, GL_FLOAT, sizeof(Vertex), (void*) (3 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 2, 3, GL_FLOAT, sizeof(Vertex), (void*) (6 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 3, 2, GL_FLOAT, sizeof(Vertex), (void*) (9 * sizeof(float)));

	// Unbind all to prevent accidentally modifying them
	m_vao.Unbind();

	//	vbo.Delete();
	vbo.Unbind();
}

void Mesh::setToBoardRoof3(float scale, int spritesWidth, int spritesHeight)
{
	float scaleGraphical = scale;
	float scalePhysical = scale;

	std::vector<std::vector<b2Vec2>> contours;

	contours.push_back(b2Vec2Vec());
	contours.back().push_back({-100.0f, -100.0f});
	contours.back().push_back({-90.0f, -100.0f});
	contours.back().push_back({-90.0f, -80.0f});
	contours.back().push_back({-30.0f, -20.0f});
	contours.back().push_back({-30.0f, 0.0f});
	contours.back().push_back({-20.0f, 10.0f});
	contours.back().push_back({0.0f, 10.0f});
	contours.back().push_back({10.0f, 20.0f});
	contours.back().push_back({0.0f, 30.0f});
	contours.back().push_back({-70.0f, 30.0f});
	contours.back().push_back({-90.0f, 50.0f});
	contours.back().push_back({-90.0f, 70.0f});
	contours.back().push_back({-70.0f, 90.0f});
	contours.back().push_back({60.0f, 90.0f});
	contours.back().push_back({90.0f, 70.0f});
	contours.back().push_back({90.0f, -100.0f});
	contours.back().push_back({100.0f, -100.0f});
	contours.back().push_back({100.0f, 100.0f});
	contours.back().push_back({-100.0f, 100.0f});

	contours.push_back(b2Vec2Vec());
	contours.back().push_back({-60.0f, 50.0f});
	contours.back().push_back({10.0f, 50.0f});
	contours.back().push_back({30.0f, 30.0f});
	contours.back().push_back({40.0f, 40.0f});
	contours.back().push_back({40.0f, 60.0f});
	contours.back().push_back({30.0f, 70.0f});
	contours.back().push_back({-60.0f, 70.0f});
	contours.back().push_back({-70.0f, 60.0f});

	contours.push_back(b2Vec2Vec());
	contours.back().push_back({50.0f, -100.0f});
	contours.back().push_back({60.0f, -100.0f});
	contours.back().push_back({60.0f, 70.0f});
	contours.back().push_back({50.0f, 60.0f});
	contours.back().push_back({50.0f, 30.0f});
	contours.back().push_back({10.0f, -10.0f});
	contours.back().push_back({-10.0f, -10.0f});
	contours.back().push_back({-10.0f, -20.0f});
	contours.back().push_back({50.0f, -80.0f});

	contours.push_back(b2Vec2Vec());
	contours.back().push_back({-70.0f, -100.0f});
	contours.back().push_back({-50.0f, -100.0f});
	contours.back().push_back({-60.0f, -90.0f});

	contours.push_back(b2Vec2Vec());
	contours.back().push_back({-30.0f, -100.0f});
	contours.back().push_back({-10.0f, -100.0f});
	contours.back().push_back({-10.0f, -50.0f});
	contours.back().push_back({-25.0f, -40.0f});
	contours.back().push_back({-30.0f, -50.0f});

	contours.push_back(b2Vec2Vec());
	contours.back().push_back({10.0f, -100.0f});
	contours.back().push_back({30.0f, -100.0f});
	contours.back().push_back({20.0f, -90.0f});

	//Triangulate the parts
	//This will be passed straight to opengl, so only one long vector is needed
	b2Vec2Vec triangulations;

	for (unsigned int contour=0; contour<contours.size(); contour++)
	{
		Vector2dVector vertices_temp;

		for (unsigned int jjj=0; jjj<contours[contour].size(); jjj++)
		{
			vertices_temp.push_back(Vector2d(contours[contour][jjj].x, contours[contour][jjj].y));
		}

		Vector2dVector result;

		Triangulate::Process(vertices_temp, result);

		for (unsigned int pair=0; pair<result.size(); pair++)
		{
			float x = result[pair].GetX();
			float y = result[pair].GetY();

			triangulations.push_back({x, y});
		}
	}


	/////////////////////////////
	// Determine the renderpath

	glmVec3Vec positions;
	std::vector<glm::vec2> textureCoordinates;

	float texcoordX = 64*7+16;
	float texcoordY = 48;

	for (unsigned int part=0; part<contours.size(); part++)
	{
		for (unsigned int corner=0; corner<contours[part].size(); corner++)
		{
			float x = contours[part][corner].x;
			float y = contours[part][corner].y;
			float z = 0.0f;

			positions.push_back({x, y, z});
			textureCoordinates.push_back({x, y});
		}
	}

	std::vector<glm::vec3> colors;
	colors.push_back(glm::vec3(0.6f, 0.4f, 0.1f));
	colors[0] *= 0.8f;

	std::vector<glm::vec3> normals;
	normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));

	/////////////////////////////
	// Determine render triangles

	std::vector<Vertex> vertices;

	for (int iii=0; iii<triangulations.size(); iii++)
	{
		float x = triangulations[iii].x;
		float y = triangulations[iii].y;
		float z = 0.0f;

		vertices.push_back({{x, y, z}, colors[0], normals[0], {x, y}});
	}

	//Store the vertices for box2d collision

	for (int part=0; part<contours.size(); part++)
	{
		m_chainShapes.push_back(std::shared_ptr<b2ChainShape>(new b2ChainShape));
		m_chainShapes.back()->CreateLoop(contours[part].data(), contours[part].size());
	}

	m_vertices = vertices;

	m_vao.Bind();

	// Generates Vertex Buffer Object and links it to vertices
	VBO vbo(m_vertices);

	// Links VBO attributes such as coordinates and colors to VAO
	m_vao.LinkAttrib(vbo, 0, 3, GL_FLOAT, sizeof(Vertex), (void*) (0 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 1, 3, GL_FLOAT, sizeof(Vertex), (void*) (3 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 2, 3, GL_FLOAT, sizeof(Vertex), (void*) (6 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 3, 2, GL_FLOAT, sizeof(Vertex), (void*) (9 * sizeof(float)));

	// Unbind all to prevent accidentally modifying them
	m_vao.Unbind();

	//	vbo.Delete();
	vbo.Unbind();
}

void Mesh::setToMovingPlatform(float width, float height, int spritesWidth, int spritesHeight)
{
	int backgroundTile=2;

	std::vector<glm::vec3> positions;

	positions.push_back({-width, -height, 0.0f});
	positions.push_back({width, -height, 0.0f});
	positions.push_back({-width, height, 0.0f});
	positions.push_back({width, height, 0.0f});

	std::vector<glm::vec3> colors;
	colors.push_back(glm::vec3(0.6f, 0.4f, 0.1f));
	colors[0] *= 0.8f;

	std::vector<glm::vec3> normals;
	normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));

	float texcoordX = 64*7+16;
	float texcoordY = 48;

	std::vector<glm::vec2> textureCoordinates;

	float left, right;
	left = backgroundTile*64;
	right = left+64;

	left /= spritesWidth;
	right /= spritesWidth;

	textureCoordinates.push_back(glm::vec2(left, 0.0f));
	textureCoordinates.push_back(glm::vec2(right, 0.0f));
	textureCoordinates.push_back(glm::vec2(left, 1.0f));
	textureCoordinates.push_back(glm::vec2(right, 1.0f));


	vertexVec vertices;
	for (int corner=0; corner<2; corner++)
	{
		vertices.push_back({positions[corner+0], colors[0], normals[0], textureCoordinates[corner+0]});
		vertices.push_back({positions[corner+1], colors[0], normals[0], textureCoordinates[corner+1]});
		vertices.push_back({positions[corner+2], colors[0], normals[0], textureCoordinates[corner+2]});
	}

	m_polygonShapes.push_back(std::shared_ptr<b2PolygonShape>(new b2PolygonShape));
	m_polygonShapes.back()->SetAsBox(width, height);

	m_vertices = vertices;

	m_vao.Bind();

	// Generates Vertex Buffer Object and links it to vertices
	VBO vbo(m_vertices);

	// Links VBO attributes such as coordinates and colors to VAO
	m_vao.LinkAttrib(vbo, 0, 3, GL_FLOAT, sizeof(Vertex), (void*) (0 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 1, 3, GL_FLOAT, sizeof(Vertex), (void*) (3 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 2, 3, GL_FLOAT, sizeof(Vertex), (void*) (6 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 3, 2, GL_FLOAT, sizeof(Vertex), (void*) (9 * sizeof(float)));

	// Unbind all to prevent accidentally modifying them
	m_vao.Unbind();

	//	vbo.Delete();
	vbo.Unbind();

}

glm::vec3 Mesh::getRandomColor()
{
	float r = float(rand())/float(RAND_MAX);
	float g = float(rand())/float(RAND_MAX);
	float b = float(rand())/float(RAND_MAX);

	return {r, g, b};
}

glmVec3Vec Mesh::triangulate(b2Vec2Vec xy)
{

}
