/*
 * Ball.h
 *
 *  Created on: Mar 5, 2022
 *      Author: mooseman
 */

#ifndef BALL_H_
#define BALL_H_

#include "Entity.h"

class Ball: public Entity {
public:
	Ball(b2World* world);
	virtual ~Ball();
};

#endif /* BALL_H_ */
