#ifndef MESHFACTORY_H
#define MESHFACTORY_H

#include <memory>

#include "Mesh.h"

//#include "glm/gtx/perpendicular.hpp"

class MeshFactory
{
public:
	enum eMesh
	{
		MESH_GROUNDFLOOR,
		MESH_SPHERE,
		MESH_PART_STRAIGHT,
		MESH_PART_TURN_RIGHT,
		MESH_PART_TURN_LEFT,

		MAX_MESH
	};

	std::string projectName;

	unsigned int partResolution;

	MeshFactory();

	glm::vec4 lerpV4(glm::vec4 p0, glm::vec4 p1, float t);
	GLfloat lerpf(GLfloat x, GLfloat y, GLfloat t);

	Mesh getGroundFloor(int length, int width, float size, glm::vec3 c1 =
	{0.1, 0.1, 0.1}, glm::vec3 c2 =
	{0.05, 0.05, 0.05});

	std::shared_ptr<Mesh> getGroundMesh(int length, int width, float size, glm::vec3 c1 =
	{0.1, 0.1, 0.1}, glm::vec3 c2 =
	{0.05, 0.05, 0.05});

	const std::string& getProjectName() const
	{
		return projectName;
	}

	void setProjectName(const std::string& projectName)
	{
		this->projectName = projectName;
	}

private:
	float m_splineLengthResolution = 10000.0f;
};

#endif // MESHFACTORY_H
