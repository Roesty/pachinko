/*
 * Entity.cpp
 *
 *  Created on: Dec 22, 2021
 *      Author: mooseman
 */

#include "Entity.h"

void Entity::draw(Shader shader)
{
	if (m_draw && m_mesh.get())
	{
		m_mesh->draw(m_physical->m_body, shader, m_debugAngle);
	}
}

Entity::Entity()
{
	m_position = glm::vec4(0.0f);
	m_position.w = 1.0f;

	m_draw = true;

	m_debugAngle = 0.0f;
}

bool Entity::isMDraw() const
{
	return m_draw;
}

void Entity::setMDraw(bool mDraw)
{
	m_draw = mDraw;
}

const std::shared_ptr<Mesh>& Entity::getMMesh() const
{
	return m_mesh;
}

void Entity::setMMesh(const std::shared_ptr<Mesh> mMesh)
{
	m_mesh = mMesh;
}

const glm::vec4& Entity::getMPosition() const
{
	std::cout<<"bruh"<<std::endl;
	if (m_physical.get())
	{
		std::cout<<"bruh2"<<std::endl;
		glm::vec4 position = m_physical->getPosition();

		float x = position.x;
		std::cout<<"bruh7 "<<x<<std::endl;
		float y = position.y;
		std::cout<<"bruh8 "<<y<<std::endl;

//		this->test.x = position.x;

		return glm::vec4(x, y, 0.0f, 0.0f);
	}
	std::cout<<"bruh3"<<std::endl;
	return m_position;
}

void Entity::setMPosition(const glm::vec4& mPosition)
{
	if (m_physical.get())
	{
		return m_physical->setPosition(mPosition);
	}
	m_position = mPosition;
}

Entity::~Entity()
{
	// TODO Auto-generated destructor stub
}

void Entity::updateBody(unsigned int VBO, b2PolygonShape body)
{
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	std::vector<float> newLocation;

	newLocation.clear();
	newLocation.push_back(body.m_vertices[0].x);
	newLocation.push_back(body.m_vertices[0].y);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(Vertex)*0, sizeof(glm::vec2), newLocation.data());

	newLocation.clear();
	newLocation.push_back(body.m_vertices[1].x);
	newLocation.push_back(body.m_vertices[1].y);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(Vertex)*1, sizeof(glm::vec2), newLocation.data());

	newLocation.clear();
	newLocation.push_back(body.m_vertices[3].x);
	newLocation.push_back(body.m_vertices[3].y);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(Vertex)*2, sizeof(glm::vec2), newLocation.data());

	newLocation.clear();
	newLocation.push_back(body.m_vertices[1].x);
	newLocation.push_back(body.m_vertices[1].y);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(Vertex)*3, sizeof(glm::vec2), newLocation.data());

	newLocation.clear();
	newLocation.push_back(body.m_vertices[3].x);
	newLocation.push_back(body.m_vertices[3].y);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(Vertex)*4, sizeof(glm::vec2), newLocation.data());

	newLocation.clear();
	newLocation.push_back(body.m_vertices[2].x);
	newLocation.push_back(body.m_vertices[2].y);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(Vertex)*5, sizeof(glm::vec2), newLocation.data());
}

























